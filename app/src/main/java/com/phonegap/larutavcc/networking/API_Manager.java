package com.phonegap.larutavcc.networking;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.phonegap.larutavcc.model.DirectorioResponse;
import com.phonegap.larutavcc.model.EventosPasadosResponse;
import com.phonegap.larutavcc.model.EventosProximosResponse;
import com.phonegap.larutavcc.model.RecomendacionesResponse;
import com.phonegap.larutavcc.model.UbicacionesResponse;
import com.phonegap.larutavcc.util.Constants;

import java.util.List;

import retrofit.Callback;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class API_Manager {

    static Gson gson = new GsonBuilder().create();

    //Singleton para ir al webservice
    private static final RestAdapter REST_ADAPTER = new RestAdapter.Builder()
            .setEndpoint(Constants.API_URL_BASE)
            .setLogLevel(RestAdapter.LogLevel.FULL.FULL)
            .setErrorHandler(ErrorHandler.DEFAULT)
            .setConverter(new com.phonegap.larutavcc.networking.GsonConverter(new GsonBuilder().create()))
            .build();

    private static final API_Interface api_interface = REST_ADAPTER.create(API_Interface.class);

    //Interface para consumir web service con Retrofit (Square)
    public static API_Interface getService() {
        return api_interface;
    }

    public interface API_Interface {

        //* Asincrono
        //Interface para consumir ubicaciones disponibles
        @GET("/{path}")
        void getUbicaciones(@Path(value = "path", encode = false) String path, Callback<List<UbicacionesResponse>> response);

//        //Interface para consumir recomendaciones por ubicacion
//        @GET("/{path}")
//        void getRecomendaciones(@Path(value = "path", encode = false) String path, @Query("location") String location,Callback<List<RecomendacionesResponse>> response);

        //Interface para consumir directorio por ubicacion
        @GET("/{path}")
        void getDirectorio(@Path(value = "path", encode = false) String path, Callback<List<DirectorioResponse>> response);

        @GET("/{path}")
        void getEventosProximos(@Path(value = "path", encode = false) String path, @Query("location") String location, Callback<List<EventosProximosResponse>> response);

        @GET("/{path}")
        void getEventosPasados(@Path(value = "path", encode = false) String path, @Query("location") String location, Callback<List<EventosPasadosResponse>> response);

//        @GET("/{path}")
//        List<RecomendacionesDetalladaResponse> getRecomendacionDetallada(@Path(value = "path", encode = false) String path, @Query("id") String id);

        //* Sincrono
        @GET("/{path}")
        List<UbicacionesResponse> getUbicaciones(@Path(value = "path", encode = false) String path);

        //Interface para consumir recomendaciones por ubicacion
        @GET("/{path}")
        List<RecomendacionesResponse> getRecomendaciones(@Path(value = "path", encode = false) String path, @Query("location") String location);

        //Interface para consumir directorio por ubicacion
        @GET("/{path}")
        List<DirectorioResponse> getDirectorio(@Path(value = "path", encode = false) String path, @Query("location") String location);

        @GET("/{path}")
        List<EventosProximosResponse> getEventosProximos(@Path(value = "path", encode = false) String path, @Query("location") String location);

        @GET("/{path}")
        List<EventosPasadosResponse> getEventosPasados(@Path(value = "path", encode = false) String path, @Query("location") String location);
    }
}