package com.phonegap.larutavcc.networking;

import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.MimeUtil;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;

/**
 * Converter to get data in charset UTF-8
 */
public class GsonConverter implements Converter {
    private static final String ENCODER = "UTF-8";
    private final Gson gson;
    private String encode;

    public GsonConverter(Gson gson) {
        this(gson, ENCODER);
    }

    public GsonConverter(Gson gson, String encode) {
        this.gson = gson;
        this.encode = encode;
    }

    @Override
    public Object fromBody(TypedInput body, Type type) throws ConversionException {
        String charset = ENCODER;
        if (body.mimeType() != null) {
            charset = MimeUtil.parseCharset(body.mimeType(), charset);
        }
        InputStreamReader isr = null;
        try {
            isr = new InputStreamReader(body.in(), charset);
//            BufferedReader br = new BufferedReader(
//                    new InputStreamReader(body.in()));
//
//            char c;
//            do {
//                c = Character.toUpperCase((char) br.read());
//                System.out.print(c);
//            } while (c != (char) -1);
//            Log.d("GSONNNN", "");
//            Charset utf8charset = Charset.forName("UTF-8");
//            Charset iso88591charset = Charset.forName("ISO-8859-1");
//            ByteBuffer inputBuffer = ByteBuffer.wrap(new byte[]{(byte)0xC3, (byte)0xA2});
//            // decode UTF-8
//            CharBuffer data = utf8charset.decode(inputBuffer);
//            // encode ISO-8559-1
//            ByteBuffer outputBuffer = iso88591charset.encode(data);
//            byte[] outputData = outputBuffer.array();

            return gson.fromJson(isr, type);
        } catch (IOException e) {
            throw new ConversionException(e);
        } catch (JsonParseException e) {
            throw new ConversionException(e);
        } finally {
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException ignored) {
                }
            }
        }
    }

    @Override
    public TypedOutput toBody(Object object) {
        try {
            return new JsonTypedOutput(gson.toJson(object).getBytes(encode), encode);
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    private static class JsonTypedOutput implements TypedOutput {
        private final byte[] jsonBytes;
        private final String encode;

        JsonTypedOutput(byte[] jsonBytes, String encode) {
            this.jsonBytes = jsonBytes;
            this.encode = encode;
        }

        @Override
        public String fileName() {
            return null;
        }

        @Override
        public String mimeType() {
            return "application/json; charset=" + encode;
        }

        @Override
        public long length() {
            return jsonBytes.length;
        }

        @Override
        public void writeTo(OutputStream out) throws IOException {
            out.write(jsonBytes);
        }
    }
}