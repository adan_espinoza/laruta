package com.phonegap.larutavcc.listener;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.TextView;

import com.phonegap.larutavcc.activity.BaseActivity;
import com.phonegap.larutavcc.activity.DashBoardActivity;
import com.phonegap.larutavcc.util.Constants;

public class PopupWindowDashboardListener implements AdapterView.OnItemClickListener {

    //Selecciona la ubicacion y se reinicia la actividad y se actualiza el parametro en shared preferences
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Context mContext = view.getContext();
        BaseActivity baseActivity = ((BaseActivity) mContext);
        String idLocation = baseActivity.locationParameter;

        // add some animation when a list item was clicked
        Animation fadeInAnimation = AnimationUtils.loadAnimation(view.getContext(), android.R.anim.fade_in);
        fadeInAnimation.setDuration(1);
        view.startAnimation(fadeInAnimation);

        // dismiss the pop up
        baseActivity.popupWindowMenu_dashboard.dismiss();

        // get the id
        String selectedItemTag = ((TextView) view).getTag().toString();

        if(!idLocation.contentEquals(selectedItemTag)) {
            baseActivity.setSharedPreferences_Location(selectedItemTag);
            Intent intent = new Intent(mContext, DashBoardActivity.class);
            intent.putExtra(Constants.EXTRA_UBICACION_ACTIVITY, true);
            baseActivity.startActivity(intent);
            baseActivity.finish();
        }
    }
}
