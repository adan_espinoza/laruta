package com.phonegap.larutavcc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.fragment.UbicacionFragment;
import com.phonegap.larutavcc.util.Constants;

/**
 * Clase que maneja UbicacionFragment al inicio de la aplicacion
 *
 * @author ariasa
 */
public class UbicacionActivity extends BaseActivity
        implements UbicacionFragment.TransferLocation_Interface {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacion);
        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        setSupportActionBar(mDashboardToolbar);
        UbicacionFragment ubicacionFragment = new UbicacionFragment();
        getFragmentManager().beginTransaction().add(R.id.fragmentContainer, ubicacionFragment).commit();
    }

    /**
     * Get parameter from UbicacionFragment and set in SharedPreferences
     */
    @Override
    public void onTransferLocation(String location) {
        setSharedPreferences_Location(location);
        Intent intent = new Intent(this,DashBoardActivity.class);
        intent.putExtra(Constants.EXTRA_UBICACION_ACTIVITY, true);
        startActivity(intent);
        finish();
    }
}
