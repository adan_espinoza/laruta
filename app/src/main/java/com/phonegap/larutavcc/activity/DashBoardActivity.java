package com.phonegap.larutavcc.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.fragment.ContactoFragment;
import com.phonegap.larutavcc.fragment.DashBoardActivityFragment;
import com.phonegap.larutavcc.fragment.PaginaWebViewFragment;
import com.phonegap.larutavcc.util.Constants;

/**
 * Actividad que maneja varios fragmentos
 * Por ahora tiene la logica de LoadingFragment, manda llamar a ContactFragment y PaginaWebViewFragment
 */
public class DashBoardActivity extends BaseActivity implements View.OnClickListener,
        ContactoFragment.WebSite_Interface {

    private boolean mDoubleBackPressedToExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        mBtnFiltro_toolbar = (ImageView) findViewById(R.id.btnFiltro_toolbar);
        mBtnFiltro_toolbar.setOnClickListener(this);
        mBtnLogo_toolbar = (ImageView) findViewById(R.id.btnLogo_toolbar);
        mBtnLogo_toolbar.setOnClickListener(this);

        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
        setSupportActionBar(mDashboardToolbar);
        if(getIntent()!=null && getIntent().getBooleanExtra(Constants.EXTRA_UBICACION_ACTIVITY,false)){
            callLoadingFragment();
        }else {
            DashBoardActivityFragment dashBoardActivityFragment = new DashBoardActivityFragment();
            getFragmentManager().beginTransaction()
                    .replace(R.id.fragmentContainer, dashBoardActivityFragment)
                    .commit();
        }
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        if (btnId == R.id.btnFiltro_toolbar) {
            escogerOtraCiudad();
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0)
            getFragmentManager().popBackStack();
        else {
            if (mDoubleBackPressedToExit) {
                super.onBackPressed();
                return;
            }
            this.mDoubleBackPressedToExit = true;
            Toast.makeText(this, getString(R.string.salir_app), Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mDoubleBackPressedToExit=false;
                }
            }, 2000);
        }
    }

    //Mandamos la url desde ContactosFragment a PaginaWebViewFragment
    @Override
    public void onTransferWebsite(String website) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PAGINAWEB_URL, website);
        PaginaWebViewFragment webViewFragment = new PaginaWebViewFragment();
        webViewFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .addToBackStack(Constants.WEBVIEW_FRAGMENT)
                .add(R.id.fragmentContainer, webViewFragment).commit();
    }
}