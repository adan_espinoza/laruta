package com.phonegap.larutavcc.activity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.DirectorioFragmentAdapter;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;

public class DirectorioActivity extends BaseActivity
        implements View.OnClickListener{

    private TextView mSeccionesTitulo;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directorio);
        mSeccionesTitulo = (TextView) findViewById(R.id.seccion_titulo_tv);
        mSeccionesTitulo.setText(getResources().getString(R.string.seccion_directorio));
        TypefaceUtils.applyFont(this);
        TypefaceUtils.applyFont(mSeccionesTitulo, true);
        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        mBtnFiltro_toolbar = (ImageView) findViewById(R.id.btnFiltro_toolbar);
        mBtnFiltro_toolbar.setOnClickListener(this);
        mBtnLogo_toolbar = (ImageView) findViewById(R.id.btnLogo_toolbar);
        mBtnLogo_toolbar.setOnClickListener(this);

        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
        setSupportActionBar(mDashboardToolbar);

        mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_verde));

        TypedArray categorias = getCategorias();
        if(categorias != null){
            // Get the ViewPager and set it's PagerAdapter so that it can display items
            mViewPager = (ViewPager) findViewById(R.id.directorio_viewpager);
            mViewPager.setAdapter(new DirectorioFragmentAdapter(getSupportFragmentManager(), this, categorias));

            // Give the TabLayout the ViewPager
            mTabLayout = (TabLayout) findViewById(R.id.categorias_tabs);
            mTabLayout.setupWithViewPager(mViewPager);
        }
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        if (btnId == R.id.btnFiltro_toolbar) {
            escogerOtraCiudad();
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }else {
            super.onBackPressed();
        }
    }

    /**
     * Metodo para obtener las categorias del directorio
     * Para Valle de G, Ruta Antigua, Puerta N se manejan unos
     * Para Ensendada, Mexicalli y Tijuana se manejan otros
     */
    private TypedArray getCategorias() {
        TypedArray categorias = null;
        if(DeviceUtils.isNumeric(locationParameter)){
            int idLocation = DeviceUtils.getIntFromString(locationParameter);
            if(idLocation >= 0 && idLocation <= 2) {
                //Valles
                categorias = getResources().obtainTypedArray(R.array.directorio_categorias_valles);
            }else if(idLocation >= 3 && idLocation <= 5) {
                //Ciudades
                categorias = getResources().obtainTypedArray(R.array.directorio_categorias_ciudades);
            }
        }
        return categorias;
    }
}
