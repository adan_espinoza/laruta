package com.phonegap.larutavcc.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.EventosFragmentAdapter;
import com.phonegap.larutavcc.util.TypefaceUtils;

public class EventosActivity extends BaseActivity implements View.OnClickListener {

    private TextView mSeccionesTitulo;
    private ViewPager mViewPager;
    private Button btnProximos, btnPasados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos);

        mSeccionesTitulo = (TextView) findViewById(R.id.seccion_titulo_tv);
        mSeccionesTitulo.setText(getResources().getString(R.string.seccion_eventos));
        mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_amarillo));
        TypefaceUtils.applyFont(this);
        TypefaceUtils.applyFont(mSeccionesTitulo, true);
        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        mBtnFiltro_toolbar = (ImageView) findViewById(R.id.btnFiltro_toolbar);
        mBtnFiltro_toolbar.setOnClickListener(this);
        mBtnLogo_toolbar = (ImageView) findViewById(R.id.btnLogo_toolbar);
        mBtnLogo_toolbar.setOnClickListener(this);

        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
        setSupportActionBar(mDashboardToolbar);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) findViewById(R.id.eventos_viewpager);
        mViewPager.setAdapter(new EventosFragmentAdapter(getFragmentManager(), this));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    btnPasados.setBackgroundResource(R.mipmap.e_pasado_off);
                    btnProximos.setBackgroundResource(R.mipmap.e_proximo_on);
                } else if (position == 1) {
                    btnPasados.setBackgroundResource(R.mipmap.e_pasado_on);
                    btnProximos.setBackgroundResource(R.mipmap.e_proximo_off);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnProximos = (Button) findViewById(R.id.btnProximosEventos);
        btnProximos.setOnClickListener(this);
        btnPasados = (Button) findViewById(R.id.btnPasadosEventos);
        btnPasados.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        switch (btnId) {
            case (R.id.btnFiltro_toolbar):
                escogerOtraCiudad();
                break;
            case (R.id.btnProximosEventos):
                mViewPager.setCurrentItem(0);
                btnPasados.setBackgroundResource(R.mipmap.e_pasado_off);
                btnProximos.setBackgroundResource(R.mipmap.e_proximo_on);
                break;
            case (R.id.btnPasadosEventos):
                mViewPager.setCurrentItem(1);
                btnPasados.setBackgroundResource(R.mipmap.e_pasado_on);
                btnProximos.setBackgroundResource(R.mipmap.e_proximo_off);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }else {
            super.onBackPressed();
        }
    }

}
