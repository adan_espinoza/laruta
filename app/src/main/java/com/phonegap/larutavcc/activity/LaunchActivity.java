package com.phonegap.larutavcc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.sql.DatabaseHelper;
import com.phonegap.larutavcc.util.DeviceUtils;

/**
 * @author snakelogan
 *         Clase que contiene la pantalla de splash. Se valida que la informacion se encuentre en la BD, sino
 *         se manda llamar al web service y se ejecuta la pantalla de seleccionar ubicacion.
 *         Tambien valida que exista red
 */
public class LaunchActivity extends BaseActivity {

    private static final long SPLASH_SCREEN_DELAY = 3000;
    private static final String LOG_TAG = LaunchActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);

        //Validar BD
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        if(databaseHelper.validateIfTableDirectorioExists()){
            Log.d(LOG_TAG, "La BD existe");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(LaunchActivity.this, DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_SCREEN_DELAY);

        }else if (isPhoneOnline()) {
            Log.d(LOG_TAG, "La BD NO existe");
            getDirectorioFromService();
            if (DeviceUtils.checkStringNullOrEmpty(getLocationParameter())) {
                //Loading information from server
                getLocationsFromService();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LaunchActivity.this, DashBoardActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, SPLASH_SCREEN_DELAY);

            } else {
                //seleccionar ubicacion
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LaunchActivity.this, UbicacionActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }, SPLASH_SCREEN_DELAY);
            }
        } else {
            showNetworkDialog();
        }
    }
}
