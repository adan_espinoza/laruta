package com.phonegap.larutavcc.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.fragment.LoginFragment;
import com.phonegap.larutavcc.util.TypefaceUtils;

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        TypefaceUtils.applyFont(this);

        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        mBtnFiltro_toolbar = (ImageView) findViewById(R.id.btnFiltro_toolbar);
        mBtnFiltro_toolbar.setOnClickListener(this);
        mBtnLogo_toolbar = (ImageView) findViewById(R.id.btnLogo_toolbar);
        mBtnLogo_toolbar.setOnClickListener(this);

        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
        setSupportActionBar(mDashboardToolbar);

        LoginFragment loginFragment = new LoginFragment();
        getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, loginFragment)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        }else {
            super.onBackPressed();
        }
    }
}
