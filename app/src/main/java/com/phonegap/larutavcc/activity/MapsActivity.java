package com.phonegap.larutavcc.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.model.DirectorioResponse;
import com.phonegap.larutavcc.model.EventosPasadosResponse;
import com.phonegap.larutavcc.model.EventosProximosResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;

import java.util.List;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {

    private static final String TAG = MapsActivity.class.getSimpleName();
    private GoogleMap mMap;
    private LatLngBounds.Builder mBuilder;
    private TextView mSeccionesTitulo;
    private List<DirectorioResponse> mDirectorioResponseList;
    private List<EventosProximosResponse> mProximosResponseList;
    private List<EventosPasadosResponse> mPasadosResponseList;
    private String mCategorias[];

    //Elementos que se piden desde otros lugares (genericos)
    private String mIdMapa;
    private String mNombre;
    private String mOrigen="";
    private String mLatitud;
    private String mLongitud;

    private boolean mostrarTodos=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        //Inicializar
        mSeccionesTitulo = (TextView) findViewById(R.id.seccion_titulo_tv);
        TypefaceUtils.applyFont(this);
        TypefaceUtils.applyFont(mSeccionesTitulo, true);

        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        mBtnFiltro_toolbar = (ImageView) findViewById(R.id.btnFiltro_toolbar);
        mBtnFiltro_toolbar.setOnClickListener(this);
        mBtnLogo_toolbar = (ImageView) findViewById(R.id.btnLogo_toolbar);
        mBtnLogo_toolbar.setOnClickListener(this);

        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
        setSupportActionBar(mDashboardToolbar);
        mDirectorioResponseList = null;//Singleton.getInstance().getDirectorioResponseList();
        switch (locationParameter){
            case Constants.LOCATION_VALLE_GUADALUPE:
            case Constants.LOCATION_PUERTA_NORTE:
            case Constants.LOCATION_RUTA_ANTIGUA:
                mCategorias = getResources().getStringArray(R.array.categorias_json_valles);
                break;
            case Constants.LOCATION_TIJUANA:
            case Constants.LOCATION_ENSENADA:
            case Constants.LOCATION_MEXICALI:
                mCategorias = getResources().getStringArray(R.array.categorias_json_ciudades);
                break;
        }

        //Validar si viene de algun otro lugar
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mIdMapa = extras.getString(Constants.ID_MAPA);
            mNombre = extras.getString(Constants.NOMBRE_MAPA);
            mOrigen = extras.getString(Constants.MAPS_ACTIVITY);
            mLatitud = extras.getString(Constants.LATITUD_MAPA);
            mLongitud = extras.getString(Constants.LONGITUD_MAPA);
            if(DeviceUtils.checkStringNullOrEmpty(mIdMapa)
                    && DeviceUtils.checkStringNullOrEmpty(mNombre)
                    && DeviceUtils.checkStringNullOrEmpty(mOrigen)
                    && DeviceUtils.checkStringNullOrEmpty(mLatitud)
                    && DeviceUtils.checkStringNullOrEmpty(mLongitud)) {
                mostrarTodos = false;
            }
        }

        switch (mOrigen) {
            case Constants.RECOMENDACIONES_DETALLE_FRAGMENT:
                mSeccionesTitulo.setText(getResources().getString(R.string.seccion_recomendaciones));
                mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_vino));
                break;
            case Constants.EVENTOS_PROXIMOS_FRAGMENT:
                mSeccionesTitulo.setText(getResources().getString(R.string.seccion_eventos));
                mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_amarillo));
                mProximosResponseList = null;//Singleton.getInstance().getEventosProximosResponseList();
                break;
            case Constants.EVENTOS_PASADOS_FRAGMENT:
                mSeccionesTitulo.setText(getResources().getString(R.string.seccion_eventos));
                mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_amarillo));
                mPasadosResponseList = null;//Singleton.getInstance().getEventosPasadosResponseList();
                break;
            case Constants.DIRECTORIO_FRAGMENT:
                mSeccionesTitulo.setText(getResources().getString(R.string.seccion_directorio));
                mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_verde));
                break;
            default:
                mSeccionesTitulo.setText(getResources().getString(R.string.seccion_mapa));
                mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_azul));
                break;

        }

        if (mDirectorioResponseList != null && !mDirectorioResponseList.isEmpty()) {
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.mapaLayout);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        if (btnId == R.id.btnFiltro_toolbar) {
            escogerOtraCiudad();
        }
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        int zoomPadding = 150;
        if (mostrarTodos) {
            addMarkers();
        } else {
            addSingleMarker();
            zoomPadding = 0;
        }
        LatLngBounds bounds = mBuilder.build();
        Log.d(TAG, bounds.toString());
        // Gets screen size
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        // Calls moveCamera passing screen size as parameters
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, zoomPadding));
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

    }

    /**
     * Agrega los marcadores de todos los elementos
     */
    private void addMarkers() {
        mMap.clear();
        mBuilder = new LatLngBounds.Builder();
        for (DirectorioResponse response : mDirectorioResponseList) {
            if(response!=null) {
                if(DeviceUtils.checkStringNullOrEmpty(response.getMapLat())
                        && DeviceUtils.checkStringNullOrEmpty(response.getMapLon())
                        && DeviceUtils.checkStringNullOrEmpty(response.getCategoryName())
                        && DeviceUtils.checkStringNullOrEmpty(response.getName())) {
                    double lat = DeviceUtils.getDoubleFromString(response.getMapLat());
                    double lon = DeviceUtils.getDoubleFromString(response.getMapLon());
                    LatLng latLng = new LatLng(lat, lon);
                    //getPinColor
                    int pin = getPinFromCategoria(response.getCategoryName());
                    if (pin != -1) {
                        mMap.addMarker(new MarkerOptions().
                                position(latLng).
                                title(DeviceUtils.convertCharset(response.getName())).
                                snippet(DeviceUtils.convertCharset(response.getCategoryName())).
                                icon(BitmapDescriptorFactory.fromResource(pin)));
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mBuilder.include(latLng);
                }
            }
        }
    }

    private int getPinFromCategoria(String categoria) {
        int resource = -1;
        if (mCategorias != null) {
            if (mCategorias[0].contentEquals(categoria)) {
                resource = R.mipmap.pin_vino;
            } else if (mCategorias[1].contentEquals(categoria)) {
                resource = R.mipmap.pin_comida;
            } else if (mCategorias[2].contentEquals(categoria)) {
                resource = R.mipmap.pin_cerveza;
            } else if (mCategorias[3].contentEquals(categoria)) {
                resource = R.mipmap.pin_mas;
            } else if (mCategorias[4].contentEquals(categoria)) {
                resource = R.mipmap.pin_tienda_vino;
            } else if (mCategorias[5].contentEquals(categoria)) {
                resource = R.mipmap.pin_hospedaje;
            }
        }
        return resource;
    }


    private void addSingleMarker() {
        mMap.clear();
        mBuilder = new LatLngBounds.Builder();
        double lat = DeviceUtils.getDoubleFromString(mLatitud);
        double lon = DeviceUtils.getDoubleFromString(mLongitud);
        LatLng latLng = new LatLng(lat, lon);
        //getPinColor comparamos con la lista del directorio
        switch (mOrigen){
            case Constants.RECOMENDACIONES_DETALLE_FRAGMENT:
                for (DirectorioResponse response : mDirectorioResponseList) {
                    if (response.getId().equals(mIdMapa)) {
                        int pin = getPinFromCategoria(response.getCategoryName());
                        mMap.addMarker(new MarkerOptions().
                                position(latLng).
                                title(DeviceUtils.convertCharset(mNombre)).
                                snippet(DeviceUtils.convertCharset(response.getCategoryName())).
                                icon(BitmapDescriptorFactory.fromResource(pin)));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mBuilder.include(latLng);
                        break;
                    }
                }
                break;
            case Constants.EVENTOS_PROXIMOS_FRAGMENT:
                for (EventosProximosResponse response : mProximosResponseList) {
                    if (response.getId().equals(mIdMapa)) {
                        int pin = getPinFromCategoria("+");
                        mMap.addMarker(new MarkerOptions().
                                position(latLng).
                                title(DeviceUtils.convertCharset(mNombre)).
                                snippet(DeviceUtils.convertCharset(getResources().getString(R.string.evento_mapa))).
                                icon(BitmapDescriptorFactory.fromResource(pin)));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mBuilder.include(latLng);
                        break;
                    }
                }
                break;
            case Constants.EVENTOS_PASADOS_FRAGMENT:
                for (EventosPasadosResponse response : mPasadosResponseList) {
                    if (response.getId().equals(mIdMapa)) {
                        int pin = getPinFromCategoria("+");
                        mMap.addMarker(new MarkerOptions().
                                position(latLng).
                                title(DeviceUtils.convertCharset(mNombre)).
                                snippet(DeviceUtils.convertCharset(getResources().getString(R.string.evento_mapa))).
                                icon(BitmapDescriptorFactory.fromResource(pin)));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mBuilder.include(latLng);
                        break;
                    }
                }
                break;
            case Constants.DIRECTORIO_FRAGMENT:
                for (DirectorioResponse response : mDirectorioResponseList) {
                    if (response.getId().equals(mIdMapa)) {
                        int pin = getPinFromCategoria(response.getCategoryName());
                        mMap.addMarker(new MarkerOptions().
                                position(latLng).
                                title(DeviceUtils.convertCharset(mNombre)).
                                snippet(DeviceUtils.convertCharset(response.getCategoryName())).
                                icon(BitmapDescriptorFactory.fromResource(pin)));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mBuilder.include(latLng);
                        break;
                    }
                }
                break;
        }



    }
}
