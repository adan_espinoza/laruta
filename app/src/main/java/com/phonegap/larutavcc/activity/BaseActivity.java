package com.phonegap.larutavcc.activity;

import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.fragment.LoadingFragment;
import com.phonegap.larutavcc.listener.PopupWindowDashboardListener;
import com.phonegap.larutavcc.model.DirectorioResponse;
import com.phonegap.larutavcc.model.UbicacionesResponse;
import com.phonegap.larutavcc.networking.API_Manager;
import com.phonegap.larutavcc.sql.DatabaseHelper;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Activity con metodos generales para toda la aplicacion
 * Las demas actividades extienden de esta
 */
public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = BaseActivity.class.getSimpleName();
    //0.- VARIABLES
    public Toolbar mDashboardToolbar;
    public ImageView mBtnFiltro_toolbar, mBtnSearch_toolbar, mBtnLogo_toolbar;
    public SharedPreferences mSavedSharedPreferences;
    public String locationParameter;
    public PopupWindow popupWindowMenu_dashboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Inicializar
        locationParameter = getLocationParameter();
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        if (btnId == R.id.btnFiltro_toolbar) {
            escogerOtraCiudad();
        }
    }

    /**
     * Metodo para generar el menu en el boton del filtro de ciudades
     *
     * @param ubicacionesResponseList
     * @return
     */
    protected PopupWindow popupWindowMenuGenerator(List<UbicacionesResponse> ubicacionesResponseList) {
        // initialize a pop up window type
        PopupWindow popupWindow = new PopupWindow(this);

        // the drop down list is a list view
        ListView listViewPopupWindow = new ListView(this);

        // set our adapter and pass our pop up window contents
        listViewPopupWindow.setAdapter(ubicacionesAdapter(ubicacionesResponseList));
        ColorDrawable whiteDivider = new ColorDrawable(getResources().getColor(R.color.color_white));
        listViewPopupWindow.setDivider(whiteDivider);
        listViewPopupWindow.setDividerHeight(1);
        // set the item click listener
        listViewPopupWindow.setOnItemClickListener(new PopupWindowDashboardListener());

        // some other visual settings
        popupWindow.setFocusable(true);
        float pixels = DeviceUtils.convertDpToPixel(202, this);
        popupWindow.setWidth((Math.round(pixels)));
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the list view as pop up window content
        popupWindow.setContentView(listViewPopupWindow);

        return popupWindow;
    }

    /**
     * Adapter para mostrar el menu de ciudades
     * No se genera en otra clase porque al pasar el adapter se pierde la vista antes de crearse (por medio de xml)
     *
     * @param listUbicaciones
     * @return
     */
    private ArrayAdapter<UbicacionesResponse> ubicacionesAdapter(final List<UbicacionesResponse> listUbicaciones) {
        ArrayAdapter<UbicacionesResponse> adapter = new ArrayAdapter<UbicacionesResponse>(this, android.R.layout.simple_list_item_1, listUbicaciones) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                // setting the ID and text for every items in the list
                String idLocation = listUbicaciones.get(position).getId().toString();
                String text = listUbicaciones.get(position).getName().toString();

                // visual settings for the list item
                TextView listItem = new TextView(getContext());
                TypefaceUtils.applyFont(getContext(), listItem);
                listItem.setText(DeviceUtils.convertCharset(text));
                listItem.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
                listItem.setTag(position);
                listItem.setTextSize(20);
                listItem.setTypeface(null, Typeface.BOLD);
                listItem.setPadding(10, 10, 10, 10);
                float height = DeviceUtils.convertDpToPixel(40, getContext());
                listItem.setHeight(Math.round(height));
                if (idLocation.contentEquals(locationParameter)) {
                    listItem.setTextColor(getResources().getColor(getLocationColor(locationParameter)));
                } else {
                    listItem.setTextColor(getResources().getColor(R.color.color_white));
                }

                return listItem;
            }
        };

        return adapter;
    }


    /**
     * Change logo de Home (ToolBar)
     */
    protected int changeLogo_btnHomeLogo_toolbar(String locationParameter) {
        int idLogo = -1;
        switch (locationParameter) {
            case Constants.LOCATION_ENSENADA:
                idLogo = R.mipmap.barra_superior_ens;
                break;
            case Constants.LOCATION_TIJUANA:
                idLogo = R.mipmap.barra_superior_tj;
                break;
            case Constants.LOCATION_MEXICALI:
                idLogo = R.mipmap.barra_superior_mxl;
                break;
            case Constants.LOCATION_VALLE_GUADALUPE:
                idLogo = R.mipmap.barra_superior_vg;
                break;
            case Constants.LOCATION_RUTA_ANTIGUA:
                idLogo = R.mipmap.barra_superior_rag;
                break;
            case Constants.LOCATION_PUERTA_NORTE:
                idLogo = R.mipmap.barra_superior_pn;
                break;
            default:
                idLogo = R.mipmap.barra_superior_home;
                break;
        }
        return idLogo;
    }

    protected int getLocationColor(String locationParameter) {
        int idColor = -1;
        switch (locationParameter) {
            case Constants.LOCATION_ENSENADA:
                idColor = R.color.color_azul;
                break;
            case Constants.LOCATION_TIJUANA:
                idColor = R.color.color_verde;
                break;
            case Constants.LOCATION_MEXICALI:
                idColor = R.color.color_amarillo;
                break;
            case Constants.LOCATION_VALLE_GUADALUPE:
            case Constants.LOCATION_RUTA_ANTIGUA:
            case Constants.LOCATION_PUERTA_NORTE:
                idColor = R.color.color_vino;
                break;
            default:
                idColor = R.color.color_white;
                break;
        }
        return idColor;
    }

    /**
     * Metodo que se utiliza para sobreescribir el parametro que contiene la ubicacion
     *
     * @param idLocation
     */
    public void setSharedPreferences_Location(String idLocation) {
        SharedPreferences.Editor editor = mSavedSharedPreferences.edit();
        editor.putString(Constants.LOCATION_PARAMETER, idLocation);
        editor.commit();
    }

    //Llamamos a LoadingFragment
    public void callLoadingFragment() {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.LOCATION_PARAMETER, getLocationParameter());
        LoadingFragment loadingFragment = new LoadingFragment();
        loadingFragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, loadingFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
    }

    /**
     * Insertar en sharedpreferences la ciudad
     *
     * @return string de ciudad
     */
    protected String getLocationParameter() {
        mSavedSharedPreferences = this.getSharedPreferences(Constants.PREFERENCES_FILE, MODE_PRIVATE);
        locationParameter = mSavedSharedPreferences.getString(Constants.LOCATION_PARAMETER, "");
        return locationParameter;
    }

    /**
     * Metodo para mostrar las ubicaciones en el menu
     */
    protected void escogerOtraCiudad() {
        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        List<UbicacionesResponse> ubicacionesResponseList = databaseHelper.getLocationsFromDB();
        popupWindowMenu_dashboard = popupWindowMenuGenerator(ubicacionesResponseList);
        int rightLocation = mDashboardToolbar.getWidth() - popupWindowMenu_dashboard.getWidth();
        popupWindowMenu_dashboard.showAsDropDown(mDashboardToolbar, rightLocation, 0);
    }

    /**
     * Metodo para validar si el telefono puede conectarse a internet
     *
     * @return
     */
    protected boolean isPhoneOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Metodo para alertar al usuario que se requiere internet y lo invitamos a salir de la app
     */
    protected void showNetworkDialog() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        String appName = getResources().getString(R.string.app_name);
        String mensaje = getResources().getString(R.string.no_internet_connection, appName);
        alertBuilder.setTitle(R.string.network_not_found);
        alertBuilder.setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    protected void getDirectorioFromService(){
        String path = getResources().getString(R.string.DIRECTORIO_URL);
        API_Manager.getService().getDirectorio(path, new Callback<List<DirectorioResponse>>() {
            @Override
            public void success(List<DirectorioResponse> directorioResponses, Response response) {
                DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                for(DirectorioResponse dirRes : directorioResponses) {
                    databaseHelper.insertDirectorioRow(dirRes);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(LOG_TAG, error.getMessage());
            }
        });
    }

    protected void getLocationsFromService(){
        String path = Constants.LOCATIONS_URL;
        API_Manager.getService().getUbicaciones(path, new Callback<List<UbicacionesResponse>>() {
            @Override
            public void success(List<UbicacionesResponse> ubicacionesResponses, Response response) {
                DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                for(UbicacionesResponse ubiResp : ubicacionesResponses) {
                    databaseHelper.insertLocationRow(ubiResp);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(LOG_TAG, error.getMessage());
            }
        });

        path = Constants.LOCATIONS_EN_URL;
        API_Manager.getService().getUbicaciones(path, new Callback<List<UbicacionesResponse>>() {
            @Override
            public void success(List<UbicacionesResponse> ubicacionesResponses, Response response) {
                DatabaseHelper databaseHelper = new DatabaseHelper(getApplicationContext());
                for(UbicacionesResponse ubiResp : ubicacionesResponses) {
                    databaseHelper.insertLocationEnRow(ubiResp);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d(LOG_TAG, error.getMessage());
            }
        });
    }

    protected void getInformationFromWebService(String locationString) {
        //Recomendaciones
//        path = getResources().getString(R.string.RECOMENDACIONES_URL);
//        API_Manager.getService().getRecomendaciones(path, locationString, new Callback<List<RecomendacionesResponse>>() {
//            @Override
//            public void success(List<RecomendacionesResponse> recomendacionesResponses, Response response) {
//                Singleton.getInstance().setRecomendacionesResponseList(recomendacionesResponses);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Log.e(LOG_TAG, error.getMessage());
//            }
//        });

        //Eventos Proximos
//        path = getResources().getString(R.string.EVENTOS_PROXIMOS_URL);
//        API_Manager.getService().getEventosProximos(path, locationString, new Callback<List<EventosProximosResponse>>() {
//            @Override
//            public void success(List<EventosProximosResponse> eventosProximosResponses, Response response) {
//                Singleton.getInstance().setEventosProximosResponseList(eventosProximosResponses);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Log.e(LOG_TAG, error.getMessage());
//            }
//        });

        //Eventos Pasados
//        path = getResources().getString(R.string.EVENTOS_PASADOS_URL);
//        API_Manager.getService().getEventosPasados(path, locationString, new Callback<List<EventosPasadosResponse>>() {
//            @Override
//            public void success(List<EventosPasadosResponse> eventosPasadosResponses, Response response) {
//                Singleton.getInstance().setEventosPasadosResponseList(eventosPasadosResponses);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                Log.e(LOG_TAG, error.getMessage());
//            }
//        });
    }

    protected void showErrorMessageFromServer() {
        //Le alertamos y lo sacamos de la app
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(BaseActivity.this);
        String mensaje = getResources().getString(R.string.network_error);
        alertBuilder.setTitle(R.string.network_error_title);
        alertBuilder.setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }
}