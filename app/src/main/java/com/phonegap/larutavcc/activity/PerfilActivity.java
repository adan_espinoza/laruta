package com.phonegap.larutavcc.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.EventosFragmentAdapter;
import com.phonegap.larutavcc.util.TypefaceUtils;

public class PerfilActivity extends BaseActivity {

    private TextView mSeccionesTitulo;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        mSeccionesTitulo = (TextView) findViewById(R.id.seccion_titulo_tv);
        mSeccionesTitulo.setText(getResources().getString(R.string.seccion_perfil));
        mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_gris));
        TypefaceUtils.applyFont(this);
        TypefaceUtils.applyFont(mSeccionesTitulo, true);
        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        mBtnFiltro_toolbar = (ImageView) findViewById(R.id.btnFiltro_toolbar);
        mBtnFiltro_toolbar.setOnClickListener(this);
        mBtnLogo_toolbar = (ImageView) findViewById(R.id.btnLogo_toolbar);
        mBtnLogo_toolbar.setOnClickListener(this);

        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
        setSupportActionBar(mDashboardToolbar);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) findViewById(R.id.perfil_viewpager);
        mViewPager.setAdapter(new EventosFragmentAdapter(getFragmentManager(), this));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
//                    btnPasados.setBackgroundResource(R.mipmap.e_pasado_off);
//                    btnProximos.setBackgroundResource(R.mipmap.e_proximo_on);
                } else if (position == 1) {
//                    btnPasados.setBackgroundResource(R.mipmap.e_pasado_on);
//                    btnProximos.setBackgroundResource(R.mipmap.e_proximo_off);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        btnProximos = (Button) findViewById(R.id.btnProximosEventos);
//        btnProximos.setOnClickListener(this);
//        btnPasados = (Button) findViewById(R.id.btnPasadosEventos);
//        btnPasados.setOnClickListener(this);
    }
}
