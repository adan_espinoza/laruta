package com.phonegap.larutavcc.activity;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.fragment.RecomendacionDetalladaFragment;
import com.phonegap.larutavcc.fragment.RecomendacionesFragment;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;

/**
 * Activity que maneja la logica de RecomendacionesFragment y RecomendacionDetalladaFragment
 */
public class RecomendacionesActivity extends BaseActivity
    implements View.OnClickListener{

    private TextView mSeccionesTitulo;
    private String mElementoID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomendaciones);

        mSeccionesTitulo = (TextView) findViewById(R.id.seccion_titulo_tv);
        TypefaceUtils.applyFont(this);
        TypefaceUtils.applyFont(mSeccionesTitulo, true);

        mDashboardToolbar = (Toolbar) findViewById(R.id.dashboardToolbar);
        mBtnFiltro_toolbar = (ImageView) findViewById(R.id.btnFiltro_toolbar);
        mBtnFiltro_toolbar.setOnClickListener(this);
        mBtnLogo_toolbar = (ImageView) findViewById(R.id.btnLogo_toolbar);
        mBtnLogo_toolbar.setOnClickListener(this);

        int idLogo = changeLogo_btnHomeLogo_toolbar(locationParameter);
        if (idLogo > 0) {
            mBtnLogo_toolbar.setImageResource(idLogo);
        }
        setSupportActionBar(mDashboardToolbar);

        if(venimosDesdeDirectorio()){
            mSeccionesTitulo.setText(getResources().getString(R.string.seccion_directorio));
            mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_verde));
            RecomendacionDetalladaFragment recomendacionDetalladaFragment = new RecomendacionDetalladaFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.RECOMENDACIONES_DETALLE_FRAGMENT, mElementoID);
            recomendacionDetalladaFragment.setArguments(bundle);
            getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, recomendacionDetalladaFragment)
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
        }else {
            mSeccionesTitulo.setText(getResources().getString(R.string.seccion_recomendaciones));
            mSeccionesTitulo.setBackgroundColor(getResources().getColor(R.color.color_vino));

            RecomendacionesFragment recomendacionesFragment = new RecomendacionesFragment();
            getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, recomendacionesFragment)
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();

        }
    }

    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        if (btnId == R.id.btnFiltro_toolbar) {
            escogerOtraCiudad();
        }
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        }else {
            super.onBackPressed();
        }
    }

    private boolean venimosDesdeDirectorio(){
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mElementoID = extras.getString(Constants.DIRECTORIO_FRAGMENT);
            if(DeviceUtils.checkStringNullOrEmpty(mElementoID)) {
                return true;
            }
        }
        return false;
    }
}
