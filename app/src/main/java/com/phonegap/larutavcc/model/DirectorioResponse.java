package com.phonegap.larutavcc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DirectorioResponse {

    @SerializedName("isShowingDirectionsButton")
    @Expose
    private String isShowingDirectionsButton;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description_long")
    @Expose
    private String descriptionLong;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("fbk_name")
    @Expose
    private String fbkName;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("map_lat")
    @Expose
    private String mapLat;
    @SerializedName("map_lon")
    @Expose
    private String mapLon;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("recommendation")
    @Expose
    private String recommendation;
    @SerializedName("description_long_en")
    @Expose
    private String descriptionLongEn;
    @SerializedName("short_description_en")
    @Expose
    private String shortDescriptionEn;
    @SerializedName("show_gallery")
    @Expose
    private String showGallery;
    @SerializedName("show_wine")
    @Expose
    private String showWine;
    @SerializedName("locations")
    @Expose
    private String locations;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("cost_en")
    @Expose
    private String costEn;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("wines")
    @Expose
    private List<List<WineResponse>> wines = new ArrayList<List<WineResponse>>();

    /**
     *
     * @return
     * The isShowingDirectionsButton
     */
    public String getIsShowingDirectionsButton() {
        return isShowingDirectionsButton;
    }

    /**
     *
     * @param isShowingDirectionsButton
     * The isShowingDirectionsButton
     */
    public void setIsShowingDirectionsButton(String isShowingDirectionsButton) {
        this.isShowingDirectionsButton = isShowingDirectionsButton;
    }

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The descriptionLong
     */
    public String getDescriptionLong() {
        return descriptionLong;
    }

    /**
     *
     * @param descriptionLong
     * The description_long
     */
    public void setDescriptionLong(String descriptionLong) {
        this.descriptionLong = descriptionLong;
    }

    /**
     *
     * @return
     * The shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     *
     * @param shortDescription
     * The short_description
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     *
     * @return
     * The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The fbkName
     */
    public String getFbkName() {
        return fbkName;
    }

    /**
     *
     * @param fbkName
     * The fbk_name
     */
    public void setFbkName(String fbkName) {
        this.fbkName = fbkName;
    }

    /**
     *
     * @return
     * The telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone
     * The telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return
     * The mapLat
     */
    public String getMapLat() {
        return mapLat;
    }

    /**
     *
     * @param mapLat
     * The map_lat
     */
    public void setMapLat(String mapLat) {
        this.mapLat = mapLat;
    }

    /**
     *
     * @return
     * The mapLon
     */
    public String getMapLon() {
        return mapLon;
    }

    /**
     *
     * @param mapLon
     * The map_lon
     */
    public void setMapLon(String mapLon) {
        this.mapLon = mapLon;
    }

    /**
     *
     * @return
     * The reference
     */
    public String getReference() {
        return reference;
    }

    /**
     *
     * @param reference
     * The reference
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     *
     * @return
     * The recommendation
     */
    public String getRecommendation() {
        return recommendation;
    }

    /**
     *
     * @param recommendation
     * The recommendation
     */
    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    /**
     *
     * @return
     * The descriptionLongEn
     */
    public String getDescriptionLongEn() {
        return descriptionLongEn;
    }

    /**
     *
     * @param descriptionLongEn
     * The description_long_en
     */
    public void setDescriptionLongEn(String descriptionLongEn) {
        this.descriptionLongEn = descriptionLongEn;
    }

    /**
     *
     * @return
     * The shortDescriptionEn
     */
    public String getShortDescriptionEn() {
        return shortDescriptionEn;
    }

    /**
     *
     * @param shortDescriptionEn
     * The short_description_en
     */
    public void setShortDescriptionEn(String shortDescriptionEn) {
        this.shortDescriptionEn = shortDescriptionEn;
    }

    /**
     *
     * @return
     * The showGallery
     */
    public String getShowGallery() {
        return showGallery;
    }

    /**
     *
     * @param showGallery
     * The show_gallery
     */
    public void setShowGallery(String showGallery) {
        this.showGallery = showGallery;
    }

    /**
     *
     * @return
     * The showWine
     */
    public String getShowWine() {
        return showWine;
    }

    /**
     *
     * @param showWine
     * The show_wine
     */
    public void setShowWine(String showWine) {
        this.showWine = showWine;
    }

    /**
     *
     * @return
     * The locations
     */
    public String getLocations() {
        return locations;
    }

    /**
     *
     * @param locations
     * The locations
     */
    public void setLocations(String locations) {
        this.locations = locations;
    }

    /**
     *
     * @return
     * The path
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     * The path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @return
     * The file
     */
    public String getFile() {
        return file;
    }

    /**
     *
     * @param file
     * The file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     *
     * @return
     * The costEn
     */
    public String getCostEn() {
        return costEn;
    }

    /**
     *
     * @param costEn
     * The cost_en
     */
    public void setCostEn(String costEn) {
        this.costEn = costEn;
    }

    /**
     *
     * @return
     * The cost
     */
    public String getCost() {
        return cost;
    }

    /**
     *
     * @param cost
     * The cost
     */
    public void setCost(String cost) {
        this.cost = cost;
    }

    /**
     *
     * @return
     * The categoryId
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     *
     * @param categoryId
     * The category_id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     *
     * @return
     * The categoryName
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     *
     * @param categoryName
     * The category_name
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     *
     * @return
     * The wines
     */
    public List<List<WineResponse>> getWines() {
        return wines;
    }

    /**
     *
     * @param wines
     * The wines
     */
    public void setWines(List<List<WineResponse>> wines) {
        this.wines = wines;
    }

}