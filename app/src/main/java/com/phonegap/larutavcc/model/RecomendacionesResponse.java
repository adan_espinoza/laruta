package com.phonegap.larutavcc.model;

import java.util.HashMap;
import java.util.Map;

public class RecomendacionesResponse {

    private String id;
    private String url_logo;
    private String path;
    private String file;
    private String image_url;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The urlLogo
     */
    public String getUrlLogo() {
        return url_logo;
    }

    /**
     *
     * @param urlLogo
     * The url_logo
     */
    public void setUrlLogo(String urlLogo) {
        this.url_logo = urlLogo;
    }

    /**
     *
     * @return
     * The path
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     * The path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @return
     * The file
     */
    public String getFile() {
        return file;
    }

    /**
     *
     * @param file
     * The file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return image_url;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.image_url = imageUrl;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}