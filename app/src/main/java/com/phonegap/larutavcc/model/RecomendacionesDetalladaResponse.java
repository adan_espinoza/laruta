package com.phonegap.larutavcc.model;

/**
 * Created by snakelogan on 10/15/15.
 */
public class RecomendacionesDetalladaResponse {

    private String id;
    private String name;
    private String url_logo;
    private String website;
    private String fbk_name;
    private String telephone;
    private String description;
    private String map_lat;
    private String map_lon;
    private String image_url;
    private String short_description;
    private String show_wine;
    private String show_gallery;
    private String file;
    private String path;
    private String reference;
    private String order;

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The urlLogo
     */
    public String getUrlLogo() {
        return url_logo;
    }

    /**
     *
     * @param urlLogo
     * The url_logo
     */
    public void setUrlLogo(String urlLogo) {
        this.url_logo = urlLogo;
    }

    /**
     *
     * @return
     * The website
     */
    public String getWebsite() {
        return website;
    }

    /**
     *
     * @param website
     * The website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     *
     * @return
     * The fbkName
     */
    public String getFbkName() {
        return fbk_name;
    }

    /**
     *
     * @param fbkName
     * The fbk_name
     */
    public void setFbkName(String fbkName) {
        this.fbk_name = fbkName;
    }

    /**
     *
     * @return
     * The telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone
     * The telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The mapLat
     */
    public String getMapLat() {
        return map_lat;
    }

    /**
     *
     * @param mapLat
     * The map_lat
     */
    public void setMapLat(String mapLat) {
        this.map_lat = mapLat;
    }

    /**
     *
     * @return
     * The mapLon
     */
    public String getMapLon() {
        return map_lon;
    }

    /**
     *
     * @param mapLon
     * The map_lon
     */
    public void setMapLon(String mapLon) {
        this.map_lon = mapLon;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return image_url;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.image_url = imageUrl;
    }

    /**
     *
     * @return
     * The shortDescription
     */
    public String getShortDescription() {
        return short_description;
    }

    /**
     *
     * @param shortDescription
     * The short_description
     */
    public void setShortDescription(String shortDescription) {
        this.short_description = shortDescription;
    }

    /**
     *
     * @return
     * The showWine
     */
    public String getShowWine() {
        return show_wine;
    }

    /**
     *
     * @param showWine
     * The show_wine
     */
    public void setShowWine(String showWine) {
        this.show_wine = showWine;
    }

    /**
     *
     * @return
     * The showGallery
     */
    public String getShowGallery() {
        return show_gallery;
    }

    /**
     *
     * @param showGallery
     * The show_gallery
     */
    public void setShowGallery(String showGallery) {
        this.show_gallery = showGallery;
    }

    /**
     *
     * @return
     * The file
     */
    public String getFile() {
        return file;
    }

    /**
     *
     * @param file
     * The file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     *
     * @return
     * The path
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     * The path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @return
     * The reference
     */
    public String getReference() {
        return reference;
    }

    /**
     *
     * @param reference
     * The reference
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

    /**
     *
     * @return
     * The order
     */
    public String getOrder() {
        return order;
    }

    /**
     *
     * @param order
     * The order
     */
    public void setOrder(String order) {
        this.order = order;
    }

}