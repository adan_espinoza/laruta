package com.phonegap.larutavcc.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.phonegap.larutavcc.util.Constants;

/**
 *
 */
public class Galeria_Vinos implements Parcelable {

    // http://multimedia.larutavcc.com/Negocios/8/Ruta_126/Galerias/BOULESENS_GTMB01.png
    // http://multimedia.larutavcc.com/Negocios/6/Ruta_443/Vinos/HESTEROENS_VTMB01.png

    private String urlCompleta;
    private String nombre;
    private String categoria;
    private String path;
    private String file;

    //added to convert black and white the images
    private boolean isColorImage = true;

    public Galeria_Vinos() {
    }

    protected Galeria_Vinos(Parcel in) {
        urlCompleta = in.readString();
        categoria = in.readString();
        path = in.readString();
        file = in.readString();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrlCompleta() {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.MULTIMEDIA_NEGOCIOS_URL_BASE)
                .append(getPath())
                .append(getCategoria())
                .append(getFile());
        if(getCategoria().contentEquals(Constants.GALERIA_CATEGORIA)){
                sb.append(Constants.GALERIA_THUMB);
        }else{
            sb.append(Constants.VINOS_THUMB);
        }
        sb.append(getNombre()).append(Constants.PNG);
        return sb.toString();
    }

    public void setUrlCompleta(String urlCompleta) {
        this.urlCompleta = urlCompleta;
    }

    public boolean isColorImage() {
        return isColorImage;
    }

    public void setIsColorImage(boolean isColorImage) {
        this.isColorImage = isColorImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(urlCompleta);
        dest.writeString(categoria);
        dest.writeString(path);
        dest.writeString(file);
        dest.writeString(nombre);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Galeria_Vinos> CREATOR = new Parcelable.Creator<Galeria_Vinos>() {
        @Override
        public Galeria_Vinos createFromParcel(Parcel in) {
            return new Galeria_Vinos(in);
        }

        @Override
        public Galeria_Vinos[] newArray(int size) {
            return new Galeria_Vinos[size];
        }
    };
}