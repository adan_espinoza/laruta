package com.phonegap.larutavcc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by snakelogan on 6/27/16.
 */
public class WineResponse {
    @SerializedName("poi_id")
    @Expose
    private String poiId;
    @SerializedName("idvino")
    @Expose
    private String idvino;
    @SerializedName("description")
    @Expose
    private String description;

    /**
     *
     * @return
     * The poiId
     */
    public String getPoiId() {
        return poiId;
    }

    /**
     *
     * @param poiId
     * The poi_id
     */
    public void setPoiId(String poiId) {
        this.poiId = poiId;
    }

    /**
     *
     * @return
     * The idvino
     */
    public String getIdvino() {
        return idvino;
    }

    /**
     *
     * @param idvino
     * The idvino
     */
    public void setIdvino(String idvino) {
        this.idvino = idvino;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

}
