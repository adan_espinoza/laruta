package com.phonegap.larutavcc.model;

import java.util.HashMap;
import java.util.Map;

public class EventosProximosResponse {

    private String id;
    private String name;
    private String url_logo;
    private String image_url;
    private String date;
    private String hour;
    private String fbk_name;
    private String telephone;
    private String cost;
    private String past_event;
    private String path;
    private String file;
    private String place;
    private String poi_id;
    private String poi_name;
    private String map_lat;
    private String map_lon;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The urlLogo
     */
    public String getUrlLogo() {
        return url_logo;
    }

    /**
     *
     * @param urlLogo
     * The url_logo
     */
    public void setUrlLogo(String urlLogo) {
        this.url_logo = urlLogo;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return image_url;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.image_url = imageUrl;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The hour
     */
    public String getHour() {
        return hour;
    }

    /**
     *
     * @param hour
     * The hour
     */
    public void setHour(String hour) {
        this.hour = hour;
    }

    /**
     *
     * @return
     * The fbkName
     */
    public String getFbkName() {
        return fbk_name;
    }

    /**
     *
     * @param fbkName
     * The fbk_name
     */
    public void setFbkName(String fbkName) {
        this.fbk_name = fbkName;
    }

    /**
     *
     * @return
     * The telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     *
     * @param telephone
     * The telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     *
     * @return
     * The cost
     */
    public String getCost() {
        return cost;
    }

    /**
     *
     * @param cost
     * The cost
     */
    public void setCost(String cost) {
        this.cost = cost;
    }

    /**
     *
     * @return
     * The pastEvent
     */
    public String getPastEvent() {
        return past_event;
    }

    /**
     *
     * @param pastEvent
     * The past_event
     */
    public void setPastEvent(String pastEvent) {
        this.past_event = pastEvent;
    }

    /**
     *
     * @return
     * The path
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @param path
     * The path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @return
     * The file
     */
    public String getFile() {
        return file;
    }

    /**
     *
     * @param file
     * The file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     *
     * @return
     * The place
     */
    public String getPlace() {
        return place;
    }

    /**
     *
     * @param place
     * The place
     */
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     *
     * @return
     * The poiId
     */
    public String getPoiId() {
        return poi_id;
    }

    /**
     *
     * @param poiId
     * The poi_id
     */
    public void setPoiId(String poiId) {
        this.poi_id = poiId;
    }

    /**
     *
     * @return
     * The poiName
     */
    public String getPoiName() {
        return poi_name;
    }

    /**
     *
     * @param poiName
     * The poi_name
     */
    public void setPoiName(String poiName) {
        this.poi_name = poiName;
    }

    /**
     *
     * @return
     * The mapLat
     */
    public String getMapLat() {
        return map_lat;
    }

    /**
     *
     * @param mapLat
     * The map_lat
     */
    public void setMapLat(String mapLat) {
        this.map_lat = mapLat;
    }

    /**
     *
     * @return
     * The mapLon
     */
    public String getMapLon() {
        return map_lon;
    }

    /**
     *
     * @param mapLon
     * The map_lon
     */
    public void setMapLon(String mapLon) {
        this.map_lon = mapLon;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}