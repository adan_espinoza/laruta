package com.phonegap.larutavcc.util;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.reflect.Field;

/**
 * Created by snakelogan on 11/8/15.
 */
public class TypefaceUtils {
    private static String LOG_TAG = TypefaceUtils.class.getSimpleName();
    private static String fontName = "CALIBRI.ttf";
    private static String fontNameBold = "CALIBRIB.ttf";
    private static Typeface fontTypeface;
    private static Typeface fontTypefaceBold;

    /**
     * Using reflection to override default typeface
     * NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT TYPEFACE WHICH WILL BE OVERRIDDEN
     *
     * @param context                    to work with assets
     * @param defaultFontNameToOverride  for example "monospace"
     * @param customFontFileNameInAssets file name of the font from assets
     */
    public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
        try {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);
            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride+":"+e.getMessage());
        }
    }

    public static void applyFont(Activity activity) {
        getFont(activity);
        traverseTree(activity.getWindow().getDecorView(), false);
    }

    public static void applyFont(Fragment fragment) {
        getFont(fragment.getActivity());
        traverseTree(fragment.getView(), false);
    }

    public static void applyFont(Context context, View view) {
        getFont(context);
        traverseTree(view, false);
    }

    public static void applyFont(ViewGroup viewGroup) {
        getFont(viewGroup.getContext());
        traverseTree(viewGroup, false);
    }

    public static void applyFont(TextView view, boolean bold) {
        getFont(view.getContext());
        traverseTree(view, bold);
    }


    public static void getFont(Context context) {
        fontTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontName);
        fontTypefaceBold = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontNameBold);
    }

//    public static Typeface getFont(Context context, boolean bold) {
//        if (fontTypeface == null) {
//            if(bold) {
//                fontTypefaceBold = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontNameBold);
//            }
//        }
//        return fontTypefaceBold;
//    }

    public static void traverseTree(View view, boolean bold) {
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;

            for (int index = 0; index < group.getChildCount(); index++) {
                traverseTree(group.getChildAt(index), bold);
            }
        } else if (view instanceof TextView) {
            TextView textview = (TextView) view;
            if(bold)
                textview.setTypeface((fontTypefaceBold));
            else
                textview.setTypeface(fontTypeface);
        }
    }
}
