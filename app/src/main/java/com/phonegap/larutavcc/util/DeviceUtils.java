package com.phonegap.larutavcc.util;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.phonegap.larutavcc.R;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by snakelogan on 10/2/15.
 */
public class DeviceUtils {

    private static String LOG_CAT = DeviceUtils.class.getCanonicalName();
    public static final String empty = "";

    public static int getScreenHeight(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    /**
     * @param context
     * @return the screen width in pixels
     */
    public static int getScreenWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    /**
     * Return true if string is not null or empty
     *
     * @param str
     * @return
     */
    public static boolean checkStringNullOrEmpty(String str) {
        if (str == null || str.isEmpty()) return false;
        else return true;
    }

    /**
     * Return true if str has info and not cero
     *
     * @param str
     * @return
     */
    public static boolean checkStringCero(String str) {
        if (str == null || str.length() <= 1 || str.contentEquals("0") || str.isEmpty())
            return false;
        else return true;
    }

    /**
     * Validate a numeric string
     *
     * @param str
     * @return true if it is number
     */
    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * @param str
     * @return integer mayor a cero sino -1 (error)
     */
    public static int getIntFromString(String str) {
        int number = -1;
        try {
            number = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return number;
        }
        return number;
    }

    /**
     * positive numbers only
     *
     * @param str
     * @return
     */
    public static double getDoubleFromString(String str) {
        double number = 0;
        try {
            number = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return number;
        }
        return number;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return px;
    }

    /**
     * Convierte el charset para que se vea correctamente la informacion
     *
     * @param input
     * @return
     */
    public static String convertCharset(String input) {
        final Charset cp1252 = Charset.forName("ISO-8859-1");
        final Charset utf8 = Charset.forName("UTF-8");
        return new String(input.getBytes(cp1252), utf8);
    }


    public static String fechaHoraConverter(String fecha, String hora, Context context) {
        //fecha y hora los recibo en este formato: "2014-04-25 17:03:13";
        StringBuilder output = new StringBuilder();
        try {
            DateFormat inputFormatDate = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat inputFormatHour = new SimpleDateFormat("HH:mm:ss");

            DateFormat outputFormatDay = new SimpleDateFormat("dd");
            DateFormat outputFormatMonth = new SimpleDateFormat("MM");
            DateFormat outputFormatYear = new SimpleDateFormat("yyyy");
            DateFormat outputFormatHour = new SimpleDateFormat("KK:mm a");
            String outputDay = outputFormatDay.format(inputFormatDate.parse(fecha));
            String outputMonth = outputFormatMonth.format(inputFormatDate.parse(fecha));
            String outputYear = outputFormatYear.format(inputFormatDate.parse(fecha));
            String outputHour = outputFormatHour.format(inputFormatHour.parse(hora));

            Date date = inputFormatDate.parse(fecha);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int dayInt = calendar.get(Calendar.DAY_OF_WEEK)-1;
            int monthInt = Integer.parseInt(outputMonth)-1;

            String[] diasList = context.getResources().getStringArray(R.array.diasSemana);
            String[] mesesList = context.getResources().getStringArray(R.array.lista_meses);

            output.append(diasList[dayInt])
                    .append(" ")
                    .append(outputDay)
                    .append("-")
                    .append(mesesList[monthInt])
                    .append("-")
                    .append(outputYear)
                    .append(" ")
                    .append(outputHour);
            Log.d(LOG_CAT, output.toString());
        } catch (ParseException e) {
            Log.e(LOG_CAT, e.getMessage());
        }
        return output.toString();
    }

    /**
     * Metodo que ingresando una fecha en formato 2016-02-20 regresa un String
     * en formato requerido para mostrar en eventos "Febrero 2016-02"
     * @param context
     * @param fecha
     * @return
     */
    public static String getMonthAndYearFromDateString(Context context, String fecha){
        StringBuilder output = new StringBuilder();
        try {
            DateFormat inputFormatDate = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outputFormatMonth = new SimpleDateFormat("MM");
            DateFormat outputFormatYear = new SimpleDateFormat("yyyy");
            String outputMonth = outputFormatMonth.format(inputFormatDate.parse(fecha));
            String outputYear = outputFormatYear.format(inputFormatDate.parse(fecha));

            Date date = inputFormatDate.parse(fecha);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            int monthInt = Integer.parseInt(outputMonth)-1;

            String[] mesesList = context.getResources().getStringArray(R.array.lista_meses);

            output.append(mesesList[monthInt])
                    .append(" ")
                    .append(outputYear)
                    .append("-")
                    .append(monthInt);
        } catch (ParseException e) {
            Log.e(LOG_CAT, e.getMessage());
        }
        return output.toString();
    }

    public static boolean validateURL(String s) {
        try {
            String pattern = "(@)?(href=')?(HREF=')?(HREF=\")?(href=\")?(http://)?[a-zA-Z_0-9\\-]+(\\.\\w[a-zA-Z_0-9\\-]+)+(/[#&\\n\\-=?\\+\\%/\\.\\w]+)?";
            //String pattern = "^(https?:\\/\\/)?(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?$";
            Pattern patt = Pattern.compile(pattern);
            Matcher matcher = patt.matcher(s);
            return matcher.matches();
        } catch (RuntimeException e) {
        }
        return false;
    }

    /**
     * Returns an image in gray scale
     * @param orginalBitmap
     * @return
     */
    public static Bitmap convertColorIntoBlackAndWhiteImage(Bitmap orginalBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(
                colorMatrix);

        Bitmap blackAndWhiteBitmap = orginalBitmap.copy(
                Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setColorFilter(colorMatrixFilter);

        Canvas canvas = new Canvas(blackAndWhiteBitmap);
        canvas.drawBitmap(blackAndWhiteBitmap, 0, 0, paint);

        return blackAndWhiteBitmap;
    }

}

