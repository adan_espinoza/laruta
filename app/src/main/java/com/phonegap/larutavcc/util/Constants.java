package com.phonegap.larutavcc.util;

/**
 * Created by snakelogan on 9/4/15.
 */
public class Constants {

    //Share preferences y variables de UbicacionFragment
    public static final String LOCATION_PARAMETER="preferences.LOCATION_PARAMETER";
    public static final String PREFERENCES_FILE="larutavcc.phongap.com.PREFERENCE_FILE_KEY";

    //Extras
    public static final String EXTRA_UBICACION_ACTIVITY = "extra.UBICACION_ACTIVITY";

    //http://api.larutavcc.com/0.99/locations
    //Static for now
    //[{"id":"0","Name":"Valle Guadalupe"},
    // {"id":"1","Name":"Ruta Antigua"},
    // {"id":"2","Name":"Puerta Norte"},
    // {"id":"3","Name":"Ensenada"},
    // {"id":"4","Name":"Tijuana"},
    // {"id":"5","Name":"Mexicali"}]
    public static final String LOCATION_VALLE_GUADALUPE = "0";
    public static final String LOCATION_RUTA_ANTIGUA = "1";
    public static final String LOCATION_PUERTA_NORTE = "2";
    public static final String LOCATION_ENSENADA = "3";
    public static final String LOCATION_TIJUANA = "4";
    public static final String LOCATION_MEXICALI = "5";

    //Netwoking
    //public static final String API_URL_BASE = "http://api.larutavcc.com/0.99";
    public static final String API_URL_BASE = "https://mxnmike.ipage.com/public_html_larutavcc/apis/1.0/";
    public static final String LOCATIONS_URL = "locations";
    public static final String LOCATIONS_EN_URL = "locations_en";
    
    //Recomendaciones Networking
    public static final String MULTIMEDIA_NEGOCIOS_URL_BASE = "http://multimedia.larutavcc.com/Negocios";
    public static final String MULTIMEDIA_EVENTOS_URL_BASE = "http://multimedia.larutavcc.com/Eventos";

    //Informacion para webviewfragment
    public static final String PAGINAWEB_URL = "PAGINAWEB_URL";
    public static final String LARUTAFACEBOOK_URL = "https://www.facebook.com/LaRutavcc";
    public static final String FACEBOOK_URL = "https://www.facebook.com/";

    //Fragments
    public static final String RECOMENDACIONES_FRAGMENT = "RECOMENDACIONES_FRAGMENT";
    public static final String RECOMENDACIONES_DETALLE_FRAGMENT = "RECOMENDACIONES_DETALLE_FRAGMENT";
    public static final String RECOMENDACIONES_DETALLE_GALERIA_FRAGMENT = "RECOMENDACIONES_DETALLE_GALERIA_FRAGMENT";
    public static final String CONTACTO_FRAGMENT = "CONTACTO_FRAGMENT";
    public static final String WEBVIEW_FRAGMENT = "WEBVIEW_FRAGMENT";
    public static final String EVENTOS_FRAGMENT = "EVENTOS_FRAGMENT";
    public static final String EVENTOS_PROXIMOS_FRAGMENT = "EVENTOS_PROXIMOS_FRAGMENT";
    public static final String EVENTOS_PASADOS_FRAGMENT = "EVENTOS_PASADOS_FRAGMENT";
    public static final String DIRECTORIO_FRAGMENT = "DIRECTORIO_FRAGMENT";
    public static final String MAPS_ACTIVITY = "MAPS_ACTIVITY";

    //Recomendaciones
    public static final String RECOMENDACIONES_FRAGMENT_IMAGEN = "_IMG_980.png";
    public static final String GALERIA_CATEGORIA = "Galerias/";
    public static final String VINOS_CATEGORIA = "Vinos/";
    public static final String GALERIA_THUMB = "_GTMB";
    public static final String GALERIA_DETALLE = "_GIMG";
    public static final String VINOS_THUMB = "_VTMB";
    public static final String VINOS_DETALLE = "_VIMG";
    public static final String PNG = ".png";
    public static final String RECOMENDACIONES_GALERIA_ID = "RECOMENDACIONES_GALERIA_ID";
    public static final String RECOMENDACIONES_GALERIA_LIST = "RECOMENDACIONES_GALERIA_LIST";

    //Directorio y Eventos Fragment
    public static final String CURRENT_TAB = "CURRENT_TAB";

    //EventosPasadosDetalleFragment
    public static final String GALERIA_EVENTOS_IMG = "_IMG";

    //MapsActivity
    public static final String LATITUD_MAPA = "LATITUD_MAPA";
    public static final String LONGITUD_MAPA = "LONGITUD_MAPA";
    public static final String ID_MAPA = "ID_MAPA";
    public static final String NOMBRE_MAPA = "NOMBRE_MAPA";
}

