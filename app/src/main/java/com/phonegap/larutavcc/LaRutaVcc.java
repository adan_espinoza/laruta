package com.phonegap.larutavcc;

import android.app.Application;

import com.phonegap.larutavcc.util.TypefaceUtils;

/**
 * Created by snakelogan on 11/8/15.
 */
public class LaRutaVcc extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // font from assets: "assets/fonts/calibri.ttf
        //TypefaceUtils.overrideFont(getApplicationContext(), "DEFAULT", "fonts/calibri.ttf");
        TypefaceUtils.overrideFont(getApplicationContext(), "serif", "fonts/CALIBRI.ttf");
    }
}
