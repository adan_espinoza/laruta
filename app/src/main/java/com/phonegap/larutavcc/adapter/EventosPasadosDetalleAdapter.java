package com.phonegap.larutavcc.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phonegap.larutavcc.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Adapter para las galerias en eventos pasados (detalle)
 */
public class EventosPasadosDetalleAdapter extends RecyclerView.Adapter<EventosPasadosDetalleAdapter.ViewHolder> {

    private List<String> mListaUrls;
    private Context mContext;
    private final LayoutInflater mInflater;

    public EventosPasadosDetalleAdapter(Context context, List<String> list) {
        mListaUrls = list;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public EventosPasadosDetalleAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.galeria_evento_pasado_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        String url = mListaUrls.get(i);
        Uri uri = Uri.parse(url);
        Picasso.with(mContext).
                load(uri).
                fit().centerCrop().noFade().
                placeholder(R.mipmap.placeholder_detalle).
                into(viewHolder.mImagen_iv);
    }

    @Override
    public int getItemCount() {
        return mListaUrls.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImagen_iv;

        public ViewHolder(View itemView) {
            super(itemView);
            mImagen_iv = (ImageView) itemView.findViewById(R.id.galeria_evento_iv);
        }
    }
}
