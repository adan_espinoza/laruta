package com.phonegap.larutavcc.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.model.Galeria_Vinos;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

/**
 * Adapter para las galerias en recomendaciones (detalle)
 */
public class RecomendacionesGaleriaAdapter extends RecyclerView.Adapter<RecomendacionesGaleriaAdapter.ViewHolder> {

    private List<Galeria_Vinos> mGaleriaList;
    private Context mContext;
    private final LayoutInflater mInflater;

    public RecomendacionesGaleriaAdapter(Context context, List<Galeria_Vinos> list){
        mGaleriaList = list;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public RecomendacionesGaleriaAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recomendaciones_galeria_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        Galeria_Vinos galeria = mGaleriaList.get(i);
        if(DeviceUtils.checkStringNullOrEmpty(galeria.getUrlCompleta())) {
            Uri uri = Uri.parse(galeria.getUrlCompleta());
            if (galeria.isColorImage()) {
                Picasso.with(mContext).
                        load(uri).
                        fit().centerCrop().noFade().
                        placeholder(R.mipmap.placeholder_logo).
                        into(viewHolder.mImagen_iv);
            } else {
                Picasso.with(mContext).
                        load(uri).noFade().
                        placeholder(R.mipmap.placeholder_logo).
                        into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                viewHolder.mImagen_iv.setImageBitmap(DeviceUtils.convertColorIntoBlackAndWhiteImage(bitmap));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
            }
        }
    }

    @Override
    public int getItemCount() {
        return mGaleriaList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView mImagen_iv;

        public ViewHolder(View itemView){
            super(itemView);
            mImagen_iv = (ImageView) itemView.findViewById(R.id.recomendaciones_galeria_iv);
        }
    }
}
