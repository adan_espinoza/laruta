package com.phonegap.larutavcc.adapter;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.activity.DirectorioActivity;
import com.phonegap.larutavcc.activity.MapsActivity;
import com.phonegap.larutavcc.activity.RecomendacionesActivity;
import com.phonegap.larutavcc.fragment.PaginaWebViewFragment;
import com.phonegap.larutavcc.model.DirectorioResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;

import java.util.List;

/**
 * Created by snakelogan
 */
public class DirectorioAdapter extends RecyclerView.Adapter<DirectorioAdapter.ViewHolder> {

    private List<DirectorioResponse> mDirectorioResponseList;
    private Context mContext;
    private final LayoutInflater mInflater;

    public DirectorioAdapter(Context context, List<DirectorioResponse> list) {
        mDirectorioResponseList = list;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public DirectorioAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.directorio_row, viewGroup, false);
        TypefaceUtils.applyFont(mContext, view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        final DirectorioResponse response = mDirectorioResponseList.get(i);

//        if(DeviceUtils.checkStringNullOrEmpty(response.getUrlLogo())) {
//            Uri uri = Uri.parse(response.getUrlLogo());
//            Picasso.with(mContext).
//                    load(uri).
//                    fit().centerCrop().noFade().
//                    placeholder(R.mipmap.placeholder_logo).
//                    into(viewHolder.mLogo_iv);
//        }
        if (DeviceUtils.checkStringNullOrEmpty(response.getName())) {
            viewHolder.mTitulo.setVisibility(View.VISIBLE);
            viewHolder.mTitulo.setText(DeviceUtils.convertCharset(response.getName()));
            TypefaceUtils.applyFont(viewHolder.mTitulo, true);
        } else {
            viewHolder.mTitulo.setVisibility(View.INVISIBLE);
            viewHolder.mTitulo.setText(DeviceUtils.empty);
        }

        if (DeviceUtils.checkStringNullOrEmpty(response.getWebsite())) {
            viewHolder.mWebsite_ll.setVisibility(View.VISIBLE);
            viewHolder.mWebsite.setText(DeviceUtils.convertCharset(response.getWebsite()));
            if (DeviceUtils.validateURL(response.getWebsite())) {
                viewHolder.mWebsite.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        getWebsiteFragment(viewHolder.mWebsite.getText().toString());
                        return false;
                    }
                });
                viewHolder.mWebsite_iv.setVisibility(View.VISIBLE);
                viewHolder.mWebsite_iv.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        getWebsiteFragment(viewHolder.mWebsite.getText().toString());
                        return false;
                    }
                });
            } else {
                viewHolder.mWebsite.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return false;
                    }
                });
                viewHolder.mWebsite_iv.setVisibility(View.INVISIBLE);
            }
        } else {
            viewHolder.mWebsite_ll.setVisibility(View.GONE);
            viewHolder.mWebsite.setText(DeviceUtils.empty);
        }

        if (DeviceUtils.checkStringNullOrEmpty(response.getFbkName().toString())) {
            viewHolder.mFacebook_ll.setVisibility(View.VISIBLE);
            viewHolder.mFacebook.setText(DeviceUtils.convertCharset(response.getFbkName()));
            viewHolder.mFacebook.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    StringBuilder facebook = new StringBuilder();
                    facebook.append(Constants.FACEBOOK_URL).append(viewHolder.mFacebook.getText().toString());
                    getWebsiteFragment(facebook.toString());
                    return false;
                }
            });
            viewHolder.mFacebook_iv.setVisibility(View.VISIBLE);
            viewHolder.mFacebook_iv.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    StringBuilder facebook = new StringBuilder();
                    facebook.append(Constants.FACEBOOK_URL).append(viewHolder.mFacebook.getText().toString());
                    getWebsiteFragment(facebook.toString());
                    return false;
                }
            });
        } else {
            viewHolder.mFacebook_ll.setVisibility(View.GONE);
            viewHolder.mFacebook.setText(DeviceUtils.empty);
        }

        if (DeviceUtils.checkStringNullOrEmpty(response.getTelephone())) {
            viewHolder.mTelefono_ll.setVisibility(View.VISIBLE);
            viewHolder.mTelefono.setText(DeviceUtils.convertCharset(response.getTelephone()));
            viewHolder.mTelefono.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    getTelefonoIntent(viewHolder.mTelefono.getText().toString());
                    return false;
                }
            });
            viewHolder.mTelefono_iv.setVisibility(View.VISIBLE);
            viewHolder.mTelefono_iv.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    getTelefonoIntent(viewHolder.mTelefono.getText().toString());
                    return false;
                }
            });
        } else {
            viewHolder.mTelefono_ll.setVisibility(View.GONE);
            viewHolder.mTelefono.setText(DeviceUtils.empty);
        }

        if (DeviceUtils.checkStringNullOrEmpty(response.getMapLat())
                && DeviceUtils.checkStringNullOrEmpty(response.getMapLon())) {
            viewHolder.mMapa_iv.setVisibility(View.VISIBLE);

            viewHolder.mMapa_iv.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    getMapaIntent(response);
                    return false;
                }
            });
        } else {
            viewHolder.mMapa_iv.setVisibility(View.INVISIBLE);
        }

        if (tieneDetalle(response.getReference())) {
            viewHolder.mMas_iv.setVisibility(View.VISIBLE);
            viewHolder.mMas_iv.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    getRecomendacionDetalleFragment(response);
                    return false;
                }
            });
        } else {
            viewHolder.mMas_iv.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mDirectorioResponseList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitulo;
        public TextView mWebsite;
        public TextView mFacebook;
        public TextView mTelefono;

        public ImageView mLogo_iv;
        public ImageView mWebsite_iv;
        public ImageView mFacebook_iv;
        public ImageView mTelefono_iv;
        public ImageView mMapa_iv;
        public ImageView mMas_iv;

        public LinearLayout mWebsite_ll;
        public LinearLayout mFacebook_ll;
        public LinearLayout mTelefono_ll;

        public ViewHolder(View itemView) {
            super(itemView);
            mLogo_iv = (ImageView) itemView.findViewById(R.id.directorio_logo_iv);
            mWebsite_iv = (ImageView) itemView.findViewById(R.id.website_iv_directorio);
            mFacebook_iv = (ImageView) itemView.findViewById(R.id.facebook_iv_directorio);
            mTelefono_iv = (ImageView) itemView.findViewById(R.id.telefono_iv_directorio);
            mMapa_iv = (ImageView) itemView.findViewById(R.id.location_dir_iv_mapa);
            mMas_iv = (ImageView) itemView.findViewById(R.id.mas_iv_detalle);

            mTitulo = (TextView) itemView.findViewById(R.id.titulo_tv_directorio);
            mWebsite = (TextView) itemView.findViewById(R.id.website_tv_directorio);
            mFacebook = (TextView) itemView.findViewById(R.id.facebook_tv_directorio);
            mTelefono = (TextView) itemView.findViewById(R.id.telefono_tv_directorio);

            mWebsite_ll = (LinearLayout) itemView.findViewById(R.id.directorio_ll_website);
            mFacebook_ll = (LinearLayout) itemView.findViewById(R.id.directorio_ll_facebook);
            mTelefono_ll = (LinearLayout) itemView.findViewById(R.id.directorio_ll_telefono);
        }
    }

    private boolean tieneDetalle(String detalle) {
        if (DeviceUtils.getIntFromString(detalle) > 0) {
            //No tiene detalle
        } else {
            return true;
        }
        return false;
    }

    public void getWebsiteFragment(String website) {
        PaginaWebViewFragment paginaWebViewFragment = new PaginaWebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PAGINAWEB_URL, website);
        paginaWebViewFragment.setArguments(bundle);

        ((DirectorioActivity) mContext).getFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, paginaWebViewFragment)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
    }

    public void getTelefonoIntent(String telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + telefono));
        mContext.startActivity(intent);
    }

    public void getMapaIntent(DirectorioResponse response) {
        Intent intent = new Intent(mContext, MapsActivity.class);
        intent.putExtra(Constants.ID_MAPA, response.getId());
        intent.putExtra(Constants.NOMBRE_MAPA, response.getName());
        intent.putExtra(Constants.LATITUD_MAPA, response.getMapLat());
        intent.putExtra(Constants.LONGITUD_MAPA, response.getMapLon());
        intent.putExtra(Constants.MAPS_ACTIVITY, Constants.DIRECTORIO_FRAGMENT);
        mContext.startActivity(intent);
    }

    public void getRecomendacionDetalleFragment(DirectorioResponse response) {
        Intent intent = new Intent(mContext, RecomendacionesActivity.class);
        intent.putExtra(Constants.DIRECTORIO_FRAGMENT, response.getId());
        mContext.startActivity(intent);
    }
}
