package com.phonegap.larutavcc.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by snakelogan on 10/2/15.
 */
public class PreCachingLayoutManagerForRecyclerView extends LinearLayoutManager {
    private static final int DEFAULT_EXTRA_LAYOUT_SPACE = 800;
    private int extraLayoutSpace = -1;
    private Context context;

    public PreCachingLayoutManagerForRecyclerView(Context context) {
        super(context);
        this.context = context;
    }

    public PreCachingLayoutManagerForRecyclerView(Context context, int extraLayoutSpace) {
        super(context);
        this.context = context;
        this.extraLayoutSpace = extraLayoutSpace;
    }

    public PreCachingLayoutManagerForRecyclerView(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        this.context = context;
    }

    public void setExtraLayoutSpace(int extraLayoutSpace) {
        this.extraLayoutSpace = extraLayoutSpace;
    }

    @Override
    protected int getExtraLayoutSpace(RecyclerView.State state) {
        if (extraLayoutSpace > 0) {
            return extraLayoutSpace;
        }
        return DEFAULT_EXTRA_LAYOUT_SPACE;
    }
}
