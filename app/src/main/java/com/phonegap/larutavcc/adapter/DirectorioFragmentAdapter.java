package com.phonegap.larutavcc.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.phonegap.larutavcc.fragment.DirectorioFragment;
import com.phonegap.larutavcc.util.DeviceUtils;

/**
 * Adapter para Tabular el directorio
 */
public class DirectorioFragmentAdapter extends FragmentPagerAdapter {

    private TypedArray mCategorias;
    private Context mContext;

    public DirectorioFragmentAdapter(FragmentManager fm, Context context, TypedArray categorias) {
        super(fm);
        mContext = context;
        mCategorias =  categorias;
    }

    @Override
    public int getCount() {
        return mCategorias.length();
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public float getPageWidth(int position) {
        return super.getPageWidth(position);
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        return DirectorioFragment.newInstance(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        //Generate size for images
        float pixels = DeviceUtils.convertDpToPixel(47,mContext);
        int intPixels = Math.round(pixels);
        // Generate title based on item position
        Drawable image = mCategorias.getDrawable(position);
        image.setBounds(0, 0, intPixels, intPixels);
        SpannableString sb = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BASELINE);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }

}
