package com.phonegap.larutavcc.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.model.Galeria_Vinos;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.List;

/**
 * Adapter para las imagenes de vino en recomendaciones (detalle)
 */
public class RecomendacionesVinoAdapter extends RecyclerView.Adapter<RecomendacionesVinoAdapter.ViewHolder> {

    private List<Galeria_Vinos> mVinosList;
    private Context mContext;
    private final LayoutInflater mInflater;

    public RecomendacionesVinoAdapter(Context context, List<Galeria_Vinos> list){
        mVinosList = list;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public RecomendacionesVinoAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recomendaciones_vinos_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        Galeria_Vinos vinos = mVinosList.get(i);
        if(DeviceUtils.checkStringNullOrEmpty(vinos.getUrlCompleta())) {
            Uri uri = Uri.parse(vinos.getUrlCompleta());
            if (vinos.isColorImage()) {
                Picasso.with(mContext).
                        load(uri).
                        fit().centerCrop().noFade().
                        placeholder(R.mipmap.placeholder_logo).
                        into(viewHolder.mImagen_iv);
            } else {
                Picasso.with(mContext).
                        load(uri).noFade().
                        placeholder(R.mipmap.placeholder_logo).
                        into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                viewHolder.mImagen_iv.setImageBitmap(DeviceUtils.convertColorIntoBlackAndWhiteImage(bitmap));
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
            }
        }
    }

    @Override
    public int getItemCount() {
        return mVinosList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView mImagen_iv;

        public ViewHolder(View itemView){
            super(itemView);
            mImagen_iv = (ImageView) itemView.findViewById(R.id.recomendaciones_vino_iv);
        }
    }
}
