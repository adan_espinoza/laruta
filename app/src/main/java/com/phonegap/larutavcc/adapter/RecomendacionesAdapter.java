package com.phonegap.larutavcc.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.model.RecomendacionesResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by snakelogan on 10/1/15.
 */
public class RecomendacionesAdapter extends RecyclerView.Adapter<RecomendacionesAdapter.ViewHolder> {

    private List<RecomendacionesResponse> mRecomendacionesResponseList;
    private Context mContext;
    private final LayoutInflater mInflater;

    public RecomendacionesAdapter(Context context, List<RecomendacionesResponse> list){
        mRecomendacionesResponseList = list;
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public RecomendacionesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recomendaciones_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        RecomendacionesResponse response = mRecomendacionesResponseList.get(i);
        if(response!=null) {
            if (DeviceUtils.checkStringNullOrEmpty(response.getUrlLogo())
                    && DeviceUtils.checkStringNullOrEmpty(response.getPath())
                    && DeviceUtils.checkStringNullOrEmpty(response.getFile())) {
                viewHolder.mImagen_iv.setVisibility(View.VISIBLE);
                viewHolder.mLogo_iv.setVisibility(View.VISIBLE);
                Uri uri = Uri.parse(response.getUrlLogo().toString());
                Picasso.with(mContext).
                        load(uri).
                        fit().centerCrop().noFade().
                        placeholder(R.mipmap.placeholder_logo).
                        into(viewHolder.mLogo_iv);

                StringBuilder url_multimedia = new StringBuilder();
                url_multimedia.append(Constants.MULTIMEDIA_NEGOCIOS_URL_BASE)
                        .append(response.getPath())
                        .append(response.getFile())
                        .append(Constants.RECOMENDACIONES_FRAGMENT_IMAGEN);
                uri = Uri.parse(url_multimedia.toString());
                Picasso.with(mContext).
                        load(uri).
                        fit().centerCrop().noFade().
                        placeholder(R.mipmap.placeholder_detalle).
                        into(viewHolder.mImagen_iv);
            } else {
                viewHolder.mContainer_ll.setVisibility(View.GONE);
                viewHolder.mImagen_iv.setVisibility(View.GONE);
                viewHolder.mLogo_iv.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mRecomendacionesResponseList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public LinearLayout mContainer_ll;
        public ImageView mLogo_iv;
        public ImageView mImagen_iv;

        public ViewHolder(View itemView){
            super(itemView);
            mLogo_iv = (ImageView) itemView.findViewById(R.id.recomendaciones_logo_iv);
            mImagen_iv = (ImageView) itemView.findViewById(R.id.recomendaciones_imagen_iv);
            mContainer_ll = (LinearLayout) itemView.findViewById(R.id.recomendaciones_row_ll_container);
        }
    }
}
