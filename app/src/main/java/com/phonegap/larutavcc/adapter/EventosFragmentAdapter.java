package com.phonegap.larutavcc.adapter;

import android.app.FragmentManager;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v13.app.FragmentPagerAdapter;

import com.phonegap.larutavcc.fragment.EventosFragment;

/**
 * Adapter para Tabular los eventos
 */
public class EventosFragmentAdapter extends FragmentPagerAdapter {

    private TypedArray mCategoriasOn, mCategoriasOff;
    private Context mContext;
    private Drawable currentChoice, notCurrentChoice;

    public EventosFragmentAdapter(FragmentManager fm, Context context){//}, TypedArray categoriasOn, TypedArray categoriasOff) {
        super(fm);
        mContext = context;
//        mCategoriasOn =  categoriasOn;
//        mCategoriasOff = categoriasOff;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public float getPageWidth(int position) {
        return super.getPageWidth(position);
    }

    @Override
    public android.app.Fragment getItem(int position) {
        return EventosFragment.newInstance(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }
}
