package com.phonegap.larutavcc.adapter;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.activity.MapsActivity;
import com.phonegap.larutavcc.fragment.EventosPasadosDetalleFragment;
import com.phonegap.larutavcc.model.EventosPasadosResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * Created by snakelogan on 10/25/15.
 */
public class EventosPasadosAdapter extends RecyclerView.Adapter<EventosPasadosAdapter.ViewHolder> {

    private ArrayList<Object> mEventosPasadosResponseList = new ArrayList<>();
    private Activity mActivity;
    private final LayoutInflater mInflater;

    public EventosPasadosAdapter(Activity activity, LinkedHashSet pasadosList) {
        Iterator iterator = pasadosList.iterator();
        while (iterator.hasNext()) {
            mEventosPasadosResponseList.add(iterator.next());
        }
        mActivity = activity;
        mInflater = LayoutInflater.from(activity);
    }

    @Override
    public EventosPasadosAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = mInflater.inflate(viewType, viewGroup, false);
        TypefaceUtils.applyFont(mActivity, view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        switch (viewHolder.getItemViewType()) {
            case R.layout.secciones_eventos_mes_row:
                String eventoCompleto = (String) mEventosPasadosResponseList.get(i);
                String[] eventoMes = eventoCompleto.split("-");
                viewHolder.mTituloMes.setText((eventoMes[0]));
                int mesInt = Integer.parseInt(eventoMes[1]);
                int resourceColor = 0;
                switch (mesInt) {
                    case 0:
                    case 6:
                        resourceColor = mActivity.getResources().getColor(R.color.color_vino);
                        break;
                    case 1:
                    case 7:
                        resourceColor = mActivity.getResources().getColor(R.color.color_amarillo);
                        break;
                    case 2:
                    case 8:
                        resourceColor = mActivity.getResources().getColor(R.color.color_verde);
                        break;
                    case 3:
                    case 9:
                        resourceColor = mActivity.getResources().getColor(R.color.color_azul);
                        break;
                    case 4:
                    case 10:
                        resourceColor = mActivity.getResources().getColor(R.color.color_rosa);
                        break;
                    case 5:
                    case 11:
                        resourceColor = mActivity.getResources().getColor(R.color.color_gris);
                        break;
                }
                viewHolder.mTituloMes.setBackgroundColor(resourceColor);
                break;
            case R.layout.eventos_row:

                final EventosPasadosResponse response = (EventosPasadosResponse) mEventosPasadosResponseList.get(i);
                if (DeviceUtils.checkStringNullOrEmpty(response.getUrlLogo())) {
                    Uri uri = Uri.parse(response.getUrlLogo());
                    Picasso.with(mActivity).
                            load(uri).
                            fit().centerCrop().noFade().
                            placeholder(R.mipmap.placeholder_logo).
                            into(viewHolder.mLogo_iv);
                    viewHolder.mLogo_iv.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            goEventosPasadosDetalleFragment(response);
                            return false;
                        }
                    });
                }
                if (DeviceUtils.checkStringNullOrEmpty(response.getName())) {
                    viewHolder.mTitulo.setVisibility(View.VISIBLE);
                    viewHolder.mTitulo.setText(DeviceUtils.convertCharset(response.getName()));
                    TypefaceUtils.applyFont(viewHolder.mTitulo, true);
                } else {
                    viewHolder.mTitulo.setVisibility(View.INVISIBLE);
                    viewHolder.mTitulo.setText(DeviceUtils.empty);
                }

                if (DeviceUtils.checkStringCero(response.getPlace())) {
                    viewHolder.mLugar.setText(DeviceUtils.convertCharset(response.getPlace()));
                } else {
                    viewHolder.mLugar.setVisibility(View.GONE);
                }

                if (DeviceUtils.checkStringCero(response.getDate()) && DeviceUtils.checkStringCero(response.getHour())) {
                    //Concatenamos fecha y hora en una sola exhibicion
                    String fecha = DeviceUtils.fechaHoraConverter(response.getDate(), response.getHour(), mActivity);
                    viewHolder.mFecha.setText(fecha);
                } else {
                    viewHolder.mFecha.setVisibility(View.GONE);
                }

                if (DeviceUtils.checkStringCero(response.getCost())) {
                    viewHolder.mCosto.setText(DeviceUtils.convertCharset(response.getCost()));
                } else {
                    viewHolder.mCosto.setVisibility(View.GONE);
                }

                if (DeviceUtils.checkStringCero(response.getTelephone())) {
                    viewHolder.mTelefono_ll.setVisibility(View.VISIBLE);
                    viewHolder.mTelefono.setText(DeviceUtils.convertCharset(response.getTelephone()));
                    viewHolder.mTelefono.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            intentTelefono(response.getTelephone());
                            return false;
                        }
                    });
                    viewHolder.mTelefono_iv.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            intentTelefono(response.getTelephone());
                            return false;
                        }
                    });
                } else {
                    viewHolder.mTelefono.setText(DeviceUtils.empty);
                    viewHolder.mTelefono_ll.setVisibility(View.GONE);
                }

                if (DeviceUtils.checkStringNullOrEmpty(response.getMapLat())
                        && DeviceUtils.checkStringNullOrEmpty(response.getMapLon())) {
                    viewHolder.mMapa_iv.setVisibility(View.VISIBLE);
                    viewHolder.mMapa_iv.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            intentMaps(response);
                            return false;
                        }
                    });
                } else {
                    viewHolder.mMapa_iv.setVisibility(View.INVISIBLE);
                }
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        int viewType=0;
        if(mEventosPasadosResponseList.get(position) instanceof String) {
            viewType = R.layout.secciones_eventos_mes_row;
        }else if(mEventosPasadosResponseList.get(position) instanceof EventosPasadosResponse) {
            viewType = R.layout.eventos_row;
        }
        return viewType;
    }

    @Override
    public int getItemCount() {
        return mEventosPasadosResponseList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitulo;
        public TextView mLugar;
        public TextView mFecha;
        public TextView mCosto;
        public TextView mTelefono;

        public ImageView mLogo_iv;
        public ImageView mTelefono_iv;
        public ImageView mMapa_iv;

        public LinearLayout mTelefono_ll;
        private TextView mTituloMes;

        public ViewHolder(View itemView) {
            super(itemView);
            mLogo_iv = (ImageView) itemView.findViewById(R.id.eventos_logo_iv);
            mTelefono_iv = (ImageView) itemView.findViewById(R.id.telefono_iv_eventos);
            mTitulo = (TextView) itemView.findViewById(R.id.titulo_tv_eventos);
            mLugar = (TextView) itemView.findViewById(R.id.lugar_tv_eventos);
            mFecha = (TextView) itemView.findViewById(R.id.fecha_tv_eventos);
            mCosto = (TextView) itemView.findViewById(R.id.costo_tv_eventos);
            mTelefono = (TextView) itemView.findViewById(R.id.telefono_tv_eventos);
            mMapa_iv = (ImageView) itemView.findViewById(R.id.location_evento_iv_mapa);
            mTelefono_ll = (LinearLayout) itemView.findViewById(R.id.eventos_ll_telefono);
            mTituloMes = (TextView) itemView.findViewById(R.id.seccion_eventos_mes_tv);
        }
    }

    private void intentTelefono(String telefono) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + telefono));
        mActivity.startActivity(intent);
    }

    private void intentMaps(EventosPasadosResponse response) {
        Intent intent = new Intent(mActivity, MapsActivity.class);
        intent.putExtra(Constants.ID_MAPA, response.getId());
        intent.putExtra(Constants.NOMBRE_MAPA, response.getName());
        intent.putExtra(Constants.LATITUD_MAPA, response.getMapLat());
        intent.putExtra(Constants.LONGITUD_MAPA, response.getMapLon());
        intent.putExtra(Constants.MAPS_ACTIVITY, Constants.EVENTOS_PASADOS_FRAGMENT);
        mActivity.startActivity(intent);
    }


    private void goEventosPasadosDetalleFragment(EventosPasadosResponse response) {
        if (DeviceUtils.getIntFromString(response.getImagenes()) > 0) {
            EventosPasadosDetalleFragment eventosDetalleFragment = new EventosPasadosDetalleFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.EVENTOS_FRAGMENT, response);
            eventosDetalleFragment.setArguments(bundle);
            mActivity.getFragmentManager().beginTransaction()
                    .add(R.id.fragmentContainer, eventosDetalleFragment)
                    .addToBackStack(Constants.RECOMENDACIONES_DETALLE_FRAGMENT)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
        }
    }

}
