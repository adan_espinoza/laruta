package com.phonegap.larutavcc.fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.PreCachingLayoutManagerForRecyclerView;
import com.phonegap.larutavcc.adapter.RecomendacionesAdapter;
import com.phonegap.larutavcc.listener.RecyclerViewItemClickListener;
import com.phonegap.larutavcc.model.RecomendacionesResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecomendacionesFragment extends Fragment {

    private List<RecomendacionesResponse> mRecomendacionesResponseList;
    private RecyclerView mRecyclerView;
    private RecomendacionesAdapter mAdapter;
    private PreCachingLayoutManagerForRecyclerView mLayoutManager;
    private TextView mEmptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mRecomendacionesResponseList = Singleton.getInstance().getRecomendacionesResponseList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recomendaciones, container, false);
        TypefaceUtils.applyFont(getActivity(),view);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recomendaciones_listview);
        mEmptyView = (TextView) view.findViewById(R.id.empty_view);
        if (mRecomendacionesResponseList != null && !mRecomendacionesResponseList.isEmpty()) {
            new LoadRecyclerViewAsyncTask().execute();
            mRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    RecomendacionesResponse response = mRecomendacionesResponseList.get(position);
                    RecomendacionDetalladaFragment recomendacionDetalladaFragment = new RecomendacionDetalladaFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.RECOMENDACIONES_DETALLE_FRAGMENT, response.getId());
                    recomendacionDetalladaFragment.setArguments(bundle);
                    getFragmentManager().beginTransaction()
                            .replace(R.id.fragmentContainer, recomendacionDetalladaFragment)
                            .addToBackStack(null)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
                }
            }));

        }else {
            mEmptyView.setVisibility(View.VISIBLE);
        }
        return view;
    }

    private void loadRecomendacionesList(){
        mAdapter = new RecomendacionesAdapter(getActivity(), mRecomendacionesResponseList);
        //Setup layout manager
        mLayoutManager = new PreCachingLayoutManagerForRecyclerView(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
    }

    private void cacheRecomendacionesList(){
        if(!mRecomendacionesResponseList.isEmpty()){
            for(RecomendacionesResponse response:mRecomendacionesResponseList){
                response = mRecomendacionesResponseList.iterator().next();
                Uri uri =  Uri.parse(response.getUrlLogo().toString());
                Picasso.with(getActivity()).load(uri).noFade().fetch();
                StringBuilder url_multimedia = new StringBuilder();
                url_multimedia.append(Constants.MULTIMEDIA_NEGOCIOS_URL_BASE)
                        .append(response.getPath())
                        .append(response.getFile())
                        .append(Constants.RECOMENDACIONES_FRAGMENT_IMAGEN);
                uri = Uri.parse(url_multimedia.toString());
                Picasso.with(getActivity()).load(uri).noFade().fetch();
            }
        }
    }

    public class LoadRecyclerViewAsyncTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            //cacheRecomendacionesList();
            loadRecomendacionesList();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if(result) {
                mRecyclerView.setHasFixedSize(true);
                mRecyclerView.setLayoutManager(mLayoutManager);
                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.setVisibility(View.VISIBLE);
            }
        }
    }
}
