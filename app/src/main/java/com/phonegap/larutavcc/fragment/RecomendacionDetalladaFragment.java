package com.phonegap.larutavcc.fragment;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.activity.MapsActivity;
import com.phonegap.larutavcc.adapter.PreCachingLayoutManagerForRecyclerView;
import com.phonegap.larutavcc.adapter.RecomendacionesGaleriaAdapter;
import com.phonegap.larutavcc.adapter.RecomendacionesVinoAdapter;
import com.phonegap.larutavcc.listener.RecyclerViewItemClickListener;
import com.phonegap.larutavcc.model.Galeria_Vinos;
import com.phonegap.larutavcc.model.RecomendacionesDetalladaResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment de RecomendacionDetallada
 * Manipulamos una lista de recomendaciones debido a mala implementacion de server: JSONOBJECT vs JSONARRAY
 */
public class RecomendacionDetalladaFragment extends Fragment
        implements View.OnClickListener {

    private String LOG_TAG = RecomendacionDetalladaFragment.class.getSimpleName();

    private String mRecomendacionID;
    private List<RecomendacionesDetalladaResponse> mRecomendacionDetalleList;
    private RecomendacionesDetalladaResponse mRecomendacionDetalle;
    private ImageView mLogo_iv;
    private ImageView mImagen_iv;
    private TextView mTitulo_tv;
    private ImageView mWebsite_iv;
    private TextView mWebsite_tv;
    private ImageView mFacebook_im;
    private TextView mFacebook_tv;
    private ImageView mTelefono_im;
    private TextView mTelefono_tv;
    private TextView mMainDescripcion_tv;
    private ImageView mMapa_iv;
    private ImageView mFavorito_iv;
    private ImageView mLista_iv;

    private RecyclerView mGaleriaRecylcerView;
    private List<Galeria_Vinos> mVinosLista;
    private RecyclerView mVinoRecyclerView;
    private RecomendacionesGaleriaAdapter mGaleriaAdapter;
    private RecomendacionesVinoAdapter mVinoAdapter;
    private PreCachingLayoutManagerForRecyclerView mLayoutManager;

    private LinearLayout mDetalleVino_ll;
    private ImageView mDetalle_vino_iv;
    private TextView mDetalle_vino_tv;
    private ImageView mCloseDetalle_vino_iv;

    private TextView mDescripcion_tv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRecomendacionID = getArguments().getString(Constants.RECOMENDACIONES_DETALLE_FRAGMENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recomendacion_detallada, container, false);
        TypefaceUtils.applyFont(getActivity(), view);
        mLogo_iv = (ImageView) view.findViewById(R.id.recomendaciones_logo_iv);
        mImagen_iv = (ImageView) view.findViewById(R.id.recomendaciones_imagen_iv);

        mTitulo_tv = (TextView) view.findViewById(R.id.titulo_tv_detalleMain);
        TypefaceUtils.applyFont(mTitulo_tv, true);
        mWebsite_iv = (ImageView) view.findViewById(R.id.website_iv_detalleMain);
        mWebsite_tv = (TextView) view.findViewById(R.id.website_tv_detalleMain);
        mFacebook_im = (ImageView) view.findViewById(R.id.facebook_iv_detalleMain);
        mFacebook_tv = (TextView) view.findViewById(R.id.facebook_tv_detalleMain);
        mTelefono_im = (ImageView) view.findViewById(R.id.telefono_iv_detalleMain);
        mTelefono_tv = (TextView) view.findViewById(R.id.telefono_tv_detalleMain);
        mMainDescripcion_tv = (TextView) view.findViewById(R.id.descripcion_tv_detalleMain);
        mMapa_iv = (ImageView) view.findViewById(R.id.location_iv_mapa);
        mFavorito_iv = (ImageView) view.findViewById(R.id.favoritos_iv);
        mLista_iv = (ImageView) view.findViewById(R.id.mi_lista_iv);

        mGaleriaRecylcerView = (RecyclerView) view.findViewById(R.id.galeria_lv);
        mVinoRecyclerView = (RecyclerView) view.findViewById(R.id.vinos_lv);

        mDescripcion_tv = (TextView) view.findViewById(R.id.descripcion_tv_detalleSecondary);

        mDetalleVino_ll = (LinearLayout) view.findViewById(R.id.detalle_vino_ll);
        mDetalle_vino_iv = (ImageView) view.findViewById(R.id.detalle_vino_iv);
        mDetalle_vino_tv = (TextView) view.findViewById(R.id.detalle_vino_tv);
        mCloseDetalle_vino_iv = (ImageView) view.findViewById(R.id.close_detalle_vino_iv);
        mCloseDetalle_vino_iv.setOnClickListener(this);

        new LoadRecomendacionAsyncTask().execute();
        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.close_detalle_vino_iv:
                resetVinosLista();
                mDetalleVino_ll.setVisibility(View.GONE);
                break;
            case R.id.website_iv_detalleMain:
                getWebsiteFragment(mWebsite_tv.getText().toString());
                break;
            case R.id.website_tv_detalleMain:
                getWebsiteFragment(mWebsite_tv.getText().toString());
                break;
            case R.id.facebook_iv_detalleMain:
                StringBuilder facebook = new StringBuilder();
                facebook.append(Constants.FACEBOOK_URL)
                        .append(mFacebook_tv.getText().toString());
                getWebsiteFragment(facebook.toString());
                break;
            case R.id.facebook_tv_detalleMain:
                facebook = new StringBuilder();
                facebook.append(Constants.FACEBOOK_URL)
                        .append(mFacebook_tv.getText().toString());
                getWebsiteFragment(facebook.toString());
                break;
            case R.id.telefono_iv_detalleMain:
                getTelefonoIntent(mTelefono_tv.getText().toString());
                break;
            case R.id.telefono_tv_detalleMain:
                getTelefonoIntent(mTelefono_tv.getText().toString());
                break;
            case R.id.location_iv_mapa:
                getMapaIntent();
                break;
        }
    }

    /**
     * Vamos al ws por la informacion de la recomendacion detallada
     */
    private void getInformationFromWebService() {
        String path = getActivity().getResources().getString(R.string.RECOMENDACIONES_DETALLADAS_URL);
       // mRecomendacionDetalleList = API_Manager.getService().getRecomendacionDetallada(path, mRecomendacionID);
    }

    /**
     * Insertamos en la vista la informacion que nos trajo el ws
     */
    private void insertarInformacionFromWebService() {
        if (mRecomendacionDetalleList.size() > 0) {
            for (int i = 0; i <= mRecomendacionDetalleList.size(); i++) {
                mRecomendacionDetalle = mRecomendacionDetalleList.get(i);
                break;
            }

            Uri uri = Uri.parse(mRecomendacionDetalle.getUrlLogo().toString());
            Picasso.with(getActivity()).
                    load(uri).
                    fit().centerCrop().noFade().
                    placeholder(R.mipmap.placeholder_logo).
                    into(mLogo_iv);
            StringBuilder url_multimedia = new StringBuilder();
            url_multimedia.append(Constants.MULTIMEDIA_NEGOCIOS_URL_BASE)
                    .append(mRecomendacionDetalle.getPath())
                    .append(mRecomendacionDetalle.getFile())
                    .append(Constants.RECOMENDACIONES_FRAGMENT_IMAGEN);
            uri = Uri.parse(url_multimedia.toString());
            Picasso.with(getActivity()).
                    load(uri).
                    fit().centerCrop().noFade().
                    placeholder(R.mipmap.placeholder_detalle).
                    into(mImagen_iv);
            mTitulo_tv.setText(DeviceUtils.convertCharset(mRecomendacionDetalle.getName()));

            if (mRecomendacionDetalle.getWebsite().contains("http") || mRecomendacionDetalle.getWebsite().contains(".com")) {
                mWebsite_iv.setVisibility(View.VISIBLE);
                mWebsite_iv.setOnClickListener(this);
                mWebsite_tv.setOnClickListener(this);
            }
            mWebsite_tv.setText(DeviceUtils.convertCharset(mRecomendacionDetalle.getWebsite()));

            mFacebook_tv.setText(DeviceUtils.convertCharset(mRecomendacionDetalle.getFbkName()));
            mFacebook_im.setOnClickListener(this);
            mFacebook_tv.setOnClickListener(this);

            mTelefono_tv.setText(DeviceUtils.convertCharset(mRecomendacionDetalle.getTelephone()));
            mTelefono_tv.setOnClickListener(this);
            mTelefono_im.setOnClickListener(this);

            mMainDescripcion_tv.setText(DeviceUtils.convertCharset(mRecomendacionDetalle.getShortDescription()));

            mMapa_iv.setOnClickListener(this);

            //Validamos si mostramos las galerias
            int show_images = Integer.parseInt(mRecomendacionDetalle.getShowGallery());
            if (show_images > 0) {
                final ArrayList<Galeria_Vinos> galeriaLista = new ArrayList<>();
                for (int i = 1; i <= show_images; i++) {
                    Galeria_Vinos galeria_vinos = new Galeria_Vinos();
                    galeria_vinos.setCategoria(Constants.GALERIA_CATEGORIA);
                    galeria_vinos.setFile(mRecomendacionDetalle.getFile());
                    galeria_vinos.setPath(mRecomendacionDetalle.getPath());
                    String stringImage = (i < 10 ? "0" : "") + i;
                    galeria_vinos.setNombre(stringImage);
                    galeriaLista.add(galeria_vinos);
                    Log.d(LOG_TAG, galeria_vinos.getUrlCompleta());
                }
                mGaleriaAdapter = new RecomendacionesGaleriaAdapter(getActivity(), galeriaLista);
                mLayoutManager = new PreCachingLayoutManagerForRecyclerView(getActivity());
                mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
                mGaleriaRecylcerView.setHasFixedSize(true);
                mGaleriaRecylcerView.setLayoutManager(mLayoutManager);
                mGaleriaRecylcerView.setAdapter(mGaleriaAdapter);
                mGaleriaRecylcerView.setVisibility(View.VISIBLE);
                mGaleriaRecylcerView.addOnItemTouchListener(new RecyclerViewItemClickListener(
                        getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        showDetalleImagen(galeriaLista, position);
                    }
                }
                ));
                mGaleriaRecylcerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                    @Override
                    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                        try {
                            int itemCount = state.getItemCount();
                            int widthPixelItem = Math.round(DeviceUtils.convertDpToPixel(73, getActivity()));
                            int widthTotalItems = widthPixelItem*itemCount;
                            int space = parent.getWidth() - widthTotalItems;
                            if (parent.getChildAdapterPosition(view) != itemCount - 1) {
                                outRect.right = space/(itemCount-1);
                            }
                        } catch (NullPointerException e) {
                            Log.d(LOG_TAG, e.getMessage());
                        }
                    }
                });
            }

            //Validamos si mostramos el vino
            show_images = Integer.parseInt(mRecomendacionDetalle.getShowWine());
            if (show_images > 0) {
                mVinosLista = new ArrayList<>();
                for (int i = 1; i <= show_images; i++) {
                    Galeria_Vinos galeria_vinos = new Galeria_Vinos();
                    galeria_vinos.setCategoria(Constants.VINOS_CATEGORIA);
                    galeria_vinos.setFile(mRecomendacionDetalle.getFile());
                    galeria_vinos.setPath(mRecomendacionDetalle.getPath());
                    String stringImage = (i < 10 ? "0" : "") + i;
                    galeria_vinos.setNombre(stringImage);
                    mVinosLista.add(galeria_vinos);
                    Log.d(LOG_TAG, galeria_vinos.getUrlCompleta());
                }
                mVinoAdapter = new RecomendacionesVinoAdapter(getActivity(), mVinosLista);
                mLayoutManager = new PreCachingLayoutManagerForRecyclerView(getActivity());
                mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                mLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
                mVinoRecyclerView.setHasFixedSize(true);
                mVinoRecyclerView.setLayoutManager(mLayoutManager);
                mVinoRecyclerView.setAdapter(mVinoAdapter);
                mVinoRecyclerView.setVisibility(View.VISIBLE);
                mVinoRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(
                        getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        //Las fotos comienzan en 1 y las posiciones en 0
                        showDetalleVino(position);
                    }
                }));
                mVinoRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
                    @Override
                    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                        try {
                            int itemCount = state.getItemCount();
                            int widthPixelItem = Math.round(DeviceUtils.convertDpToPixel(60, getActivity()));
                            int widthTotalItems = widthPixelItem*itemCount;
                            int space = parent.getWidth() - widthTotalItems;
                            if (parent.getChildAdapterPosition(view) != itemCount - 1) {
                                outRect.right = space/(itemCount-1);
                            }
                        } catch (NullPointerException e) {
                            Log.d(LOG_TAG, e.getMessage());
                        }
                    }
                });

            }
            mDescripcion_tv.setText(DeviceUtils.convertCharset(mRecomendacionDetalle.getDescription()));
        }
    }

    private void showDetalleVino(int posicion) {
//      http://multimedia.larutavcc.com/Negocios/6/Ruta_443/Vinos/HESTEROENS_VIMG01.png
        mDetalleVino_ll.setVisibility(View.VISIBLE);
        StringBuilder url_multimedia = new StringBuilder();
        int indiceImagen = posicion+1;
        String stringImage = (indiceImagen < 10 ? "0" : "") + indiceImagen;
        url_multimedia.append(Constants.MULTIMEDIA_NEGOCIOS_URL_BASE)
                .append(mRecomendacionDetalle.getPath())
                .append(Constants.VINOS_CATEGORIA)
                .append(mRecomendacionDetalle.getFile())
                .append(Constants.VINOS_DETALLE)
                .append(stringImage)
                .append(Constants.PNG);
        Log.d(LOG_TAG, url_multimedia.toString());
        Uri uri = Uri.parse(url_multimedia.toString());
        Picasso.with(getActivity()).
                load(uri).
                fit().noFade().
                placeholder(R.mipmap.placeholder_logo).
                into(mDetalle_vino_iv);
        mDetalle_vino_tv.setText(DeviceUtils.convertCharset("No available"));

        for(int i=0; i<mVinosLista.size(); i++){
            if(i==posicion){
                mVinosLista.get(i).setIsColorImage(true);
            }else {
                mVinosLista.get(i).setIsColorImage(false);
            }
        }
        mVinoAdapter.notifyDataSetChanged();
    }

    private void resetVinosLista(){
        for(int i=0; i<mVinosLista.size(); i++){
            mVinosLista.get(i).setIsColorImage(true);
        }
        mVinoAdapter.notifyDataSetChanged();
    }

    private void showDetalleImagen(ArrayList<Galeria_Vinos> lista, int indiceImagen) {
//      http://multimedia.larutavcc.com/Negocios/6/Ruta_443/Galerias/HESTEROENS_GIMG01.png
        GaleriaRecomendacionFragment galeriaRecomendacionFragment = new GaleriaRecomendacionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.RECOMENDACIONES_GALERIA_ID, indiceImagen);
        bundle.putParcelableArrayList(Constants.RECOMENDACIONES_GALERIA_LIST, lista);
        galeriaRecomendacionFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragmentContainer, galeriaRecomendacionFragment)
                .addToBackStack(Constants.RECOMENDACIONES_DETALLE_GALERIA_FRAGMENT)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
    }


    public class LoadRecomendacionAsyncTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {
            if (DeviceUtils.checkStringNullOrEmpty(mRecomendacionID)) {
                getInformationFromWebService();
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                insertarInformacionFromWebService();
            }
        }
    }


    public void getWebsiteFragment(String website){
        PaginaWebViewFragment paginaWebViewFragment = new PaginaWebViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PAGINAWEB_URL, website);
        paginaWebViewFragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .add(R.id.fragmentContainer, paginaWebViewFragment)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
    }

    public void getTelefonoIntent(String telefono){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + telefono));
        getActivity().startActivity(intent);
    }

    public void getMapaIntent(){
        Intent intent = new Intent(getActivity(), MapsActivity.class);
        intent.putExtra(Constants.ID_MAPA, mRecomendacionDetalle.getId());
        intent.putExtra(Constants.NOMBRE_MAPA, mRecomendacionDetalle.getName());
        intent.putExtra(Constants.LATITUD_MAPA, mRecomendacionDetalle.getMapLat());
        intent.putExtra(Constants.LONGITUD_MAPA, mRecomendacionDetalle.getMapLon());
        intent.putExtra(Constants.MAPS_ACTIVITY, Constants.RECOMENDACIONES_DETALLE_FRAGMENT);
        startActivity(intent);
    }
}
