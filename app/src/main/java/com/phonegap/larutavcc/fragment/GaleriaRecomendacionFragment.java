package com.phonegap.larutavcc.fragment;


import android.app.Fragment;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.PreCachingLayoutManagerForRecyclerView;
import com.phonegap.larutavcc.adapter.RecomendacionesGaleriaAdapter;
import com.phonegap.larutavcc.listener.RecyclerViewItemClickListener;
import com.phonegap.larutavcc.model.Galeria_Vinos;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Fragment para galeria de las imagenes en recomendaciones (detalle)
 */
public class GaleriaRecomendacionFragment extends Fragment implements View.OnClickListener {

    private String LOG_TAG = GaleriaRecomendacionFragment.class.getSimpleName();

    private ImageView mClose_iv;
    private ImageView mCurrentImage_iv;
    private RecyclerView mGaleriaRecylcerView;
    private int mPosicion;
    private ArrayList<Galeria_Vinos> mGaleriaList;
    private RecomendacionesGaleriaAdapter mAdapter;
    private PreCachingLayoutManagerForRecyclerView mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPosicion = getArguments().getInt(Constants.RECOMENDACIONES_GALERIA_ID);
        mGaleriaList = getArguments().getParcelableArrayList(Constants.RECOMENDACIONES_GALERIA_LIST);
        if(mGaleriaList!=null){
            updateGaleriaList();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_galeria_recomendacion, container, false);
        mClose_iv = (ImageView) view.findViewById(R.id.close_detalle_imagen_iv);
        mClose_iv.setOnClickListener(this);
        mCurrentImage_iv = (ImageView) view.findViewById(R.id.currentImage_iv);
        mGaleriaRecylcerView = (RecyclerView) view.findViewById(R.id.galeria_lv);
        getCurrentImage(mGaleriaList.get(mPosicion), mPosicion);
        generarLista();
        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.close_detalle_imagen_iv:
                getActivity().getFragmentManager().popBackStack();
                break;
        }
    }

    private void generarLista() {
//      http://multimedia.larutavcc.com/Negocios/8/Ruta_126/Galerias/BOULESENS_GTMB01.png
        mAdapter = new RecomendacionesGaleriaAdapter(getActivity(), mGaleriaList);
        mLayoutManager = new PreCachingLayoutManagerForRecyclerView(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
        mGaleriaRecylcerView.setHasFixedSize(true);
        mGaleriaRecylcerView.setLayoutManager(mLayoutManager);
        mGaleriaRecylcerView.setAdapter(mAdapter);
        mGaleriaRecylcerView.addOnItemTouchListener(new RecyclerViewItemClickListener(
                getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (mPosicion != position) {
                    Galeria_Vinos galeria = mGaleriaList.get(position);
                    mPosicion = position;
                    getCurrentImage(galeria, position);
                    updateGaleriaList();
                    mAdapter.notifyDataSetChanged();
                }
            }
        }
        ));
        mGaleriaRecylcerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                try {
                    int itemCount = state.getItemCount();
                    int widthPixelItem = Math.round(DeviceUtils.convertDpToPixel(73, getActivity()));
                    int widthTotalItems = widthPixelItem * itemCount;
                    int space = parent.getWidth() - widthTotalItems;
                    if (parent.getChildAdapterPosition(view) != itemCount - 1) {
                        outRect.right = space / (itemCount - 1);
                    }
                } catch (NullPointerException e) {
                    Log.d(LOG_TAG, e.getMessage());
                }
            }
        });
    }

    private void getCurrentImage(Galeria_Vinos galeria, int indiceImagen) {
//  http://multimedia.larutavcc.com/Negocios/6/Ruta_443/Galerias/HESTEROENS_GIMG01.png
        if(DeviceUtils.checkStringNullOrEmpty(galeria.getPath())
                && DeviceUtils.checkStringNullOrEmpty(galeria.getFile())) {

            StringBuilder url_multimedia = new StringBuilder();
            indiceImagen++;
            String stringImage = (indiceImagen < 10 ? "0" : "") + indiceImagen;
            url_multimedia.append(Constants.MULTIMEDIA_NEGOCIOS_URL_BASE)
                    .append(galeria.getPath())
                    .append(Constants.GALERIA_CATEGORIA)
                    .append(galeria.getFile())
                    .append(Constants.GALERIA_DETALLE)
                    .append(stringImage)
                    .append(Constants.PNG);
            Uri uri = Uri.parse(url_multimedia.toString());
            Log.d(LOG_TAG, url_multimedia.toString());
            Picasso.with(getActivity()).
                    load(uri).
                    fit().noFade().placeholder(R.mipmap.placeholder_detalle).
                    into(mCurrentImage_iv);
        }
    }

    private void updateGaleriaList(){
        for(int i=0; i<mGaleriaList.size(); i++){
            if(i==mPosicion){
                mGaleriaList.get(i).setIsColorImage(true);
            }else {
                mGaleriaList.get(i).setIsColorImage(false);
            }
        }
    }
}
