package com.phonegap.larutavcc.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.activity.DirectorioActivity;
import com.phonegap.larutavcc.activity.EventosActivity;
import com.phonegap.larutavcc.activity.LoginActivity;
import com.phonegap.larutavcc.activity.MapsActivity;
import com.phonegap.larutavcc.activity.RecomendacionesActivity;
import com.phonegap.larutavcc.util.Constants;

/**
 * Fragmento que manda llamar a otros fragmentos o actividades
 */
public class DashBoardActivityFragment extends Fragment implements View.OnClickListener{

    View view;
    ImageButton btnRecomendaciones, btnMapa, btnDirectorio, btnEventos, btnPerfil, btnContacto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_dash_board, container, false);

        //Inicializar
        btnRecomendaciones = (ImageButton) view.findViewById(R.id.btn_recomendaciones);
            btnRecomendaciones.setOnClickListener(this);
        btnMapa = (ImageButton) view.findViewById(R.id.btn_mapa);
            btnMapa.setOnClickListener(this);
        btnDirectorio = (ImageButton) view.findViewById(R.id.btn_directorio);
            btnDirectorio.setOnClickListener(this);
        btnEventos = (ImageButton) view.findViewById(R.id.btn_eventos);
            btnEventos.setOnClickListener(this);
        btnPerfil = (ImageButton) view.findViewById(R.id.btn_perfil);
            btnPerfil.setOnClickListener(this);
        btnContacto = (ImageButton) view.findViewById(R.id.btn_logoContacto);
            btnContacto.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        int idBtn = v.getId();
        Intent intent;
        switch (idBtn){
            case(R.id.btn_recomendaciones):
                intent = new Intent(getActivity(), RecomendacionesActivity.class);
                getActivity().startActivity(intent);
                break;
            case(R.id.btn_mapa):
                intent = new Intent(getActivity(), MapsActivity.class);
                getActivity().startActivity(intent);
                break;
            case(R.id.btn_directorio):
                intent = new Intent(getActivity(), DirectorioActivity.class);
                getActivity().startActivity(intent);
                break;
            case(R.id.btn_eventos):
                intent = new Intent(getActivity(), EventosActivity.class);
                getActivity().startActivity(intent);
                break;
            case(R.id.btn_perfil):
                intent = new Intent(getActivity(), LoginActivity.class);
                getActivity().startActivity(intent);
                break;
            case(R.id.btn_logoContacto):
                ContactoFragment contactoFragment = new ContactoFragment();
                getFragmentManager().beginTransaction()
                        .addToBackStack(Constants.CONTACTO_FRAGMENT)
                        .add(R.id.fragmentContainer, contactoFragment).commit();
                break;
        }
    }


}
