package com.phonegap.larutavcc.fragment;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.model.DirectorioResponse;
import com.phonegap.larutavcc.model.EventosPasadosResponse;
import com.phonegap.larutavcc.model.EventosProximosResponse;
import com.phonegap.larutavcc.model.RecomendacionesResponse;
import com.phonegap.larutavcc.model.UbicacionesResponse;
import com.phonegap.larutavcc.networking.API_Manager;
import com.phonegap.larutavcc.util.Constants;

import java.util.List;

/**
 * Implementa la logica del api para ir por la informacion al server
 */
public class LoadingFragment extends Fragment {

    private static final String LOG_TAG = LoadingFragment.class.getSimpleName();

    View view;
    String locationString = "";
    ImageView imageView_barraLoading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_loading, container, false);
        imageView_barraLoading = (ImageView) view.findViewById(R.id.imageView_barraLoading);
        if (getArguments() != null) {
            Bundle locationBundle = getArguments();
            locationString = locationBundle.get(Constants.LOCATION_PARAMETER).toString();
        }
        setBackgroundLoadingFragment();
        new LoadingTask().execute();
        return view;
    }

    private void setBackgroundLoadingFragment() {
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView_backgroundLoading);
        int idBackgroundLoading = -1;
        switch (locationString) {
            case Constants.LOCATION_ENSENADA:
                idBackgroundLoading = R.mipmap.cargando_ens;
                break;
            case Constants.LOCATION_TIJUANA:
                idBackgroundLoading = R.mipmap.cargando_tj;
                break;
            case Constants.LOCATION_MEXICALI:
                idBackgroundLoading = R.mipmap.cargando_mxl;
                break;
            case Constants.LOCATION_VALLE_GUADALUPE:
                idBackgroundLoading = R.mipmap.cargando_vg;
                break;
            case Constants.LOCATION_RUTA_ANTIGUA:
                idBackgroundLoading = R.mipmap.cargando_ra;
                break;
            case Constants.LOCATION_PUERTA_NORTE:
                idBackgroundLoading = R.mipmap.cargando_pn;
                break;
            default:
                idBackgroundLoading = R.mipmap.login_page;//En caso de no encontrar parametro
                break;
        }
        imageView.setImageResource(idBackgroundLoading);
    }

    private boolean getUbicacionesFromService() {
        String path = getActivity().getResources().getString(R.string.UBICACIONES_URL);
        try {
            List<UbicacionesResponse> ubicacionesResponseList = API_Manager.getService().getUbicaciones(path);
            if (ubicacionesResponseList != null) {
//                Singleton.getInstance().setUbicacionesResponseList(ubicacionesResponseList);
                return true;
            }
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
        }
        return false;
    }

    private boolean getDirectorioFromService() {
        String path = getActivity().getResources().getString(R.string.DIRECTORIO_URL);
        try {
            List<DirectorioResponse> directorioResponseList = API_Manager.getService().getDirectorio(path, locationString);
            if (directorioResponseList != null) {
//                Singleton.getInstance().setDirectorioResponseList(directorioResponseList);
                return true;
            }
        }catch (Exception e){
            Log.d(LOG_TAG, e.getMessage());
        }
        return false;
    }

    private boolean getRecomendacionesFromService() {
        String path = getActivity().getResources().getString(R.string.RECOMENDACIONES_URL);
        try {
            List<RecomendacionesResponse> recomendacionesResponseList =
                    API_Manager.getService().getRecomendaciones(path, locationString);
            if (recomendacionesResponseList != null) {
//                Singleton.getInstance().setRecomendacionesResponseList(recomendacionesResponseList);
                return true;
            }
        }catch (Exception e){
            Log.d(LOG_TAG, e.getMessage());
        }
        return false;
    }

    private boolean getEventosPasadosFromServer() {
        String path = getActivity().getResources().getString(R.string.EVENTOS_PASADOS_URL);
        try {
            List<EventosPasadosResponse> eventosPasadosResponseList =
                    API_Manager.getService().getEventosPasados(path, locationString);
            if (eventosPasadosResponseList != null) {
//                Singleton.getInstance().setEventosPasadosResponseList(eventosPasadosResponseList);
                return true;
            }
        }catch (Exception e){
            Log.d(LOG_TAG, e.getMessage());
        }
        return false;
    }

    private boolean getEventosProximosFromServer() {
        String path = getActivity().getResources().getString(R.string.EVENTOS_PROXIMOS_URL);
        try {
            List<EventosProximosResponse> eventosProximosResponseList =
                    API_Manager.getService().getEventosProximos(path, locationString);
            if (eventosProximosResponseList != null) {
//                Singleton.getInstance().setEventosProximosResponseList(eventosProximosResponseList);
                return true;
            }
        }catch (Exception e){
            Log.d(LOG_TAG, e.getMessage());
        }
        return false;
    }

    private void showLoadingBar(String value) {
        String loadingBar = "loading_barra0" + value;
        int resID = getActivity().getResources().getIdentifier(loadingBar, "mipmap", getActivity().getPackageName());
        imageView_barraLoading.setImageResource(resID);
    }

    private void showErrorMessage() {
        //Le alertamos y lo sacamos de la app
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        String mensaje = getResources().getString(R.string.network_error);
        alertBuilder.setTitle(R.string.network_error_title);
        alertBuilder.setMessage(mensaje)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getActivity().finish();
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }


    private class LoadingTask extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(this, 1000);
                }
            }, 1000);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            for (int i = 2; i < 7; i++) {
                switch (i) {
                    case 2:
                        if(getUbicacionesFromService()) {
                            publishProgress(i);
                        }else {
                            return false;
                        }
                        break;
                    case 3:
                        if(getDirectorioFromService()) {
                            publishProgress(i);
                        }else {
                            return false;
                        }
                        break;
                    case 4:
                        if (getRecomendacionesFromService()) {
                            publishProgress(i);
                        } else {
                            return false;
                        }
                        break;
                    case 5:
                        if (getEventosPasadosFromServer()) {
                            publishProgress(i);
                        } else {
                            return false;
                        }
                        break;
                    case 6:
                        if (getEventosProximosFromServer()) {
                            publishProgress(i);
                        } else {
                            return false;
                        }
                        break;
                }
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            if (result) {
                showLoadingBar(String.valueOf(7));//Last value of loading bar
                DashBoardActivityFragment dashBoardActivityFragment = new DashBoardActivityFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragmentContainer, dashBoardActivityFragment)
                        .commit();
            } else {
                showErrorMessage();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            showLoadingBar(values[0].toString());
        }
    }

}
