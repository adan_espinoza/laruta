package com.phonegap.larutavcc.fragment;


import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.TypefaceUtils;

public class ContactoFragment extends Fragment implements View.OnClickListener {

    TextView textView_websiteLink, textView_emailLink, textView_facebookLink, textView_telefonoLink;
    ImageView imageView_websiteLink, imageView_emailLink, imageView_facebookLink, imageView_telefonoLink;
    WebSite_Interface webSite_interface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webSite_interface = (WebSite_Interface) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Inicializar
        View view = inflater.inflate(R.layout.fragment_contacto, container, false);
        TypefaceUtils.applyFont(getActivity(), view);
        textView_websiteLink = (TextView) view.findViewById(R.id.textView_websiteLink);
        textView_websiteLink.setOnClickListener(this);

        textView_emailLink = (TextView) view.findViewById(R.id.textView_emailLink);
        textView_emailLink.setOnClickListener(this);

        textView_facebookLink = (TextView) view.findViewById(R.id.textView_facebookLink);
        textView_facebookLink.setOnClickListener(this);

        textView_telefonoLink = (TextView) view.findViewById(R.id.textView_telefonoLink);
        textView_telefonoLink.setOnClickListener(this);

        imageView_websiteLink = (ImageView) view.findViewById(R.id.imageView_websiteLink);
        imageView_websiteLink.setOnClickListener(this);

        imageView_emailLink = (ImageView) view.findViewById(R.id.imageView_emailLink);
        imageView_emailLink.setOnClickListener(this);

        imageView_facebookLink = (ImageView) view.findViewById(R.id.imageView_facebookLink);
        imageView_facebookLink.setOnClickListener(this);

        imageView_telefonoLink = (ImageView) view.findViewById(R.id.imageView_telefonoLink);
        imageView_telefonoLink.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        int idClick = v.getId();
        if(idClick == R.id.textView_websiteLink || idClick == R.id.imageView_websiteLink){
            //Ir al website
            String website = getResources().getString(R.string.larutaWebsite);
            webSite_interface.onTransferWebsite(website);
        }else if(idClick == R.id.textView_emailLink || idClick == R.id.imageView_emailLink){
            //Mandar correo
            String subject = getResources().getString(R.string.mailSubject);
            String emailTo = getResources().getString(R.string.larutaEmail);
            String versionFrom = getResources().getString(R.string.versionInfo)+
                   Build.MANUFACTURER+"_"+Build.MODEL+"_"+String.valueOf(Build.VERSION.SDK_INT);
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailTo});
            intent.putExtra(Intent.EXTRA_TEXT, versionFrom);
            getActivity().startActivity(intent);
        }else if(idClick == R.id.textView_facebookLink || idClick == R.id.imageView_facebookLink){
            //Ir a facebook
            String website = Constants.LARUTAFACEBOOK_URL;
            webSite_interface.onTransferWebsite(website);
        }else if(idClick == R.id.textView_telefonoLink || idClick == R.id.imageView_telefonoLink){
            //Ir al dialer
            Intent intent = new Intent(Intent.ACTION_DIAL);
            String telefono =  getResources().getString(R.string.larutaTelefono);
            intent.setData(Uri.parse("tel:" + telefono));
            getActivity().startActivity(intent);
        }
    }

    public interface WebSite_Interface{
        void onTransferWebsite(String website);
    }
}
