package com.phonegap.larutavcc.fragment;


import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.EventosPasadosDetalleAdapter;
import com.phonegap.larutavcc.adapter.PreCachingLayoutManagerForRecyclerView;
import com.phonegap.larutavcc.model.EventosPasadosResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Fragment con imagenes de eventos pasados
 */
public class EventosPasadosDetalleFragment extends Fragment {
    private String LOG_TAG = EventosPasadosDetalleFragment.class.getSimpleName();

    private EventosPasadosResponse mPasadoResponse;
    private List<String> mUrls;

    private TextView mTituloEvento;
    private ImageView mHeaderEvento;

    private RecyclerView mEventosRecycler;
    private EventosPasadosDetalleAdapter mAdapter;
    private PreCachingLayoutManagerForRecyclerView mLayoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPasadoResponse = (EventosPasadosResponse) getArguments().getSerializable(Constants.EVENTOS_FRAGMENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_eventos_pasados_detalle, container, false);
        TypefaceUtils.applyFont(getActivity(),view);
        mHeaderEvento = (ImageView) view.findViewById(R.id.header_evento_iv);
        mTituloEvento = (TextView) view.findViewById(R.id.titulo_evento_tv);
        mEventosRecycler= (RecyclerView) view.findViewById(R.id.eventos_pasados_list);
        if (mPasadoResponse != null) {
            mTituloEvento.setText(DeviceUtils.convertCharset(mPasadoResponse.getName()));
            TypefaceUtils.applyFont(mTituloEvento, true);
            generateList(DeviceUtils.getIntFromString(mPasadoResponse.getImagenes()));
            getImageHeader();
            mUrls.remove(0);
            getImageList();
        }

        return view;
    }

    /**
     * Generamos un array de strings con las urls completas de las imagenes
     * @param images
     */
    private void generateList(int images) {
//  http://multimedia.larutavcc.com/Eventos/2015/August/Evento_590/SALONVINO15_IMG00.png
        mUrls = new ArrayList<>();
        for (int i = 0; i < images; i++) {
            String indice = (i < 10 ? "0" : "") + i;
            StringBuilder sb = new StringBuilder();
            sb.append(Constants.MULTIMEDIA_EVENTOS_URL_BASE)
                    .append(mPasadoResponse.getPath())
                    .append(mPasadoResponse.getFile())
                    .append(Constants.GALERIA_EVENTOS_IMG)
                    .append(indice)
                    .append(Constants.PNG);
            mUrls.add(sb.toString());
            Log.d(LOG_TAG,sb.toString());
        }
    }

    /**
     * Obtenemos el encabezado
     */
    private void getImageHeader() {
        //EL encabezado es la primer imagen de la lista
        Uri uri =  Uri.parse(mUrls.get(0));
        Picasso.with(getActivity()).
                load(uri).
                fit().centerCrop().noFade().
                placeholder(R.mipmap.placeholder_detalle).
                into(mHeaderEvento);
    }

    /**
     * Vamos al adapter para llenar el recyclerview
     */
    private void getImageList(){
        mAdapter = new EventosPasadosDetalleAdapter(getActivity(), mUrls);
        mLayoutManager = new PreCachingLayoutManagerForRecyclerView(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));
        mEventosRecycler.setHasFixedSize(true);
        mEventosRecycler.setLayoutManager(mLayoutManager);
        mEventosRecycler.setAdapter(mAdapter);
    }


}
