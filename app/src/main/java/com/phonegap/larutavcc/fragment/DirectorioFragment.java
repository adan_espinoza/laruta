package com.phonegap.larutavcc.fragment;


import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.DirectorioAdapter;
import com.phonegap.larutavcc.adapter.PreCachingLayoutManagerForRecyclerView;
import com.phonegap.larutavcc.model.DirectorioResponse;
import com.phonegap.larutavcc.sql.DatabaseHelper;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Contiene la logica de todas las tabs, vamos a discriminar la lista de directorio por categorias en un solo fragment
 * para las seis vistas
 */
public class DirectorioFragment extends Fragment {

    private int mIdTab;
    private TextView mEmptyView;
    private RecyclerView mRecyclerView;
    private DirectorioAdapter mAdapter;
    private PreCachingLayoutManagerForRecyclerView mLayoutManager;
    private ImageView mHeaderCategoria;
    private List<DirectorioResponse> mDirectorioResponseList;
    private List<DirectorioResponse> mCategoriaList;
    private String [] mDiscriminarCategoria;
    private SharedPreferences mSavedSharedPreferences;
    private String locationParameter;

    public static DirectorioFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(Constants.CURRENT_TAB, page);
        DirectorioFragment fragment = new DirectorioFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSavedSharedPreferences = getActivity().getSharedPreferences(Constants.PREFERENCES_FILE, getActivity().MODE_PRIVATE);
        locationParameter = mSavedSharedPreferences.getString(Constants.LOCATION_PARAMETER, "");
        mIdTab = getArguments().getInt(Constants.CURRENT_TAB);
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        mDirectorioResponseList = databaseHelper.getDirectorioFromBD(locationParameter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_directorio, container, false);
        TypefaceUtils.applyFont(getActivity(), view);
        mHeaderCategoria = (ImageView) view.findViewById(R.id.header_categoria_iv);
        setHeaderCategoria();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.directorio_listview);
        mEmptyView = (TextView) view.findViewById(R.id.empty_view);
        if (mDirectorioResponseList != null && !mDirectorioResponseList.isEmpty()) {
            setListaCategoria();
        } else {
            mEmptyView.setVisibility(View.VISIBLE);
        }
        return view;
    }

    /**
     * Metodo para colocar el nombre del header para categoria seleccionada
     */
    private void setHeaderCategoria() {
        TypedArray categorias = null;
        switch (locationParameter){
            case Constants.LOCATION_VALLE_GUADALUPE:
            case Constants.LOCATION_PUERTA_NORTE:
            case Constants.LOCATION_RUTA_ANTIGUA:
                categorias = getResources().obtainTypedArray(R.array.directorio_header_valles);
                mDiscriminarCategoria = new String[categorias.length()];
                mDiscriminarCategoria = getResources().getStringArray(R.array.categorias_json_valles);
                break;
            case Constants.LOCATION_TIJUANA:
            case Constants.LOCATION_ENSENADA:
            case Constants.LOCATION_MEXICALI:
                categorias = getResources().obtainTypedArray(R.array.directorio_header_ciudades);
                mDiscriminarCategoria = new String[categorias.length()];
                mDiscriminarCategoria = getResources().getStringArray(R.array.categorias_json_ciudades);
                break;
        }
        mHeaderCategoria.setBackgroundDrawable(categorias.getDrawable(mIdTab));
        categorias.recycle();
    }

    /**
     * Inyectamos la lista dependiendo la categoria elegida
     */
    private void setListaCategoria() {
        mCategoriaList = new ArrayList<>();

        for (DirectorioResponse directorioResponse : mDirectorioResponseList) {
            String discriminator = directorioResponse.getCategoryName();
            if (mDiscriminarCategoria[mIdTab].contentEquals(discriminator)) {
                mCategoriaList.add(directorioResponse);
            }
        }

        mAdapter = new DirectorioAdapter(getActivity(), mCategoriaList);
        //Setup layout manager
        mLayoutManager = new PreCachingLayoutManagerForRecyclerView(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
    }
}
