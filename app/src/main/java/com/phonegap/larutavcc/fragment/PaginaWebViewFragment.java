package com.phonegap.larutavcc.fragment;


import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.util.Constants;

public class PaginaWebViewFragment extends Fragment {

    View view;
    WebView webView_internetWindow;
    String paginaWeb;
    ProgressDialog progressDialog;
    Activity activity;
    private static final String LOG_TAG = PaginaWebViewFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_web_view, container, false);
        webView_internetWindow = (WebView) view.findViewById(R.id.webView_internetWindow);

        if(getArguments()!=null) {
            Bundle bundle = getArguments();
            paginaWeb = bundle.get(Constants.PAGINAWEB_URL).toString();
            if(!paginaWeb.contains("http")) {
                paginaWeb = "http://"+paginaWeb;
            }
            Log.d(LOG_TAG, "PaginaWeb:" + paginaWeb);
            webView_internetWindow.getSettings().setJavaScriptEnabled(true);
            String userAgent = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
            webView_internetWindow.getSettings().setUserAgentString(userAgent);
            webView_internetWindow.setWebViewClient(new MyWebViewClient());
            String cargando = getResources().getString(R.string.cargandoWebView)+" "+paginaWeb;
            String espere = getResources().getString(R.string.espereWebView);
            progressDialog = ProgressDialog.show(getActivity(),espere,cargando, true, true,
                    new DialogInterface.OnCancelListener(){
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            dialog.cancel();
                        }
                    });
            webView_internetWindow.loadUrl(paginaWeb);

        }
        return view;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d(LOG_TAG, "Termino la descarga de URL: " + url);
            if (progressDialog!=null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Log.d(LOG_TAG, "Error con WebView: " + error.toString());
            String errorWebView = getResources().getString(R.string.errorWebView);
            Toast.makeText(getActivity(), errorWebView, Toast.LENGTH_SHORT).show();
        }
    }
}
