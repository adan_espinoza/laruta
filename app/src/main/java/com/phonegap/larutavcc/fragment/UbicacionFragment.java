package com.phonegap.larutavcc.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.util.Constants;

public class UbicacionFragment extends Fragment implements View.OnClickListener{

    TransferLocation_Interface transferLocation_interface;
    ImageButton btnEnsenada, btnTijuana, btnMexicali, btnValleGua, btnRutaAnt, btnPuertaN;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        transferLocation_interface = (TransferLocation_Interface) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ubicacion, container, false);
        btnEnsenada = (ImageButton) view.findViewById(R.id.btn_ensenada_ubicacion);
            btnEnsenada.setOnClickListener(this);
        btnTijuana = (ImageButton) view.findViewById(R.id.btn_tijuana_ubicacion);
            btnTijuana.setOnClickListener(this);
        btnMexicali = (ImageButton) view.findViewById(R.id.btn_mexicali_ubicacion);
            btnMexicali.setOnClickListener(this);
        btnValleGua = (ImageButton) view.findViewById(R.id.btn_vallegua_ubicacion);
            btnValleGua.setOnClickListener(this);
        btnRutaAnt = (ImageButton) view.findViewById(R.id.btn_rutaant_ubicacion);
            btnRutaAnt.setOnClickListener(this);
        btnPuertaN = (ImageButton) view.findViewById(R.id.btn_puertan_ubicacion);
            btnPuertaN.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        int btnId = v.getId();
        switch (btnId){
            case R.id.btn_ensenada_ubicacion:
                transferLocation_interface.onTransferLocation(Constants.LOCATION_ENSENADA);
            break;
            case R.id.btn_tijuana_ubicacion:
                transferLocation_interface.onTransferLocation(Constants.LOCATION_TIJUANA);
            break;
            case R.id.btn_mexicali_ubicacion:
                transferLocation_interface.onTransferLocation(Constants.LOCATION_MEXICALI);
            break;
            case R.id.btn_vallegua_ubicacion:
                transferLocation_interface.onTransferLocation(Constants.LOCATION_VALLE_GUADALUPE);
            break;
            case R.id.btn_rutaant_ubicacion:
                transferLocation_interface.onTransferLocation(Constants.LOCATION_RUTA_ANTIGUA);
            break;
            case R.id.btn_puertan_ubicacion:
                transferLocation_interface.onTransferLocation(Constants.LOCATION_PUERTA_NORTE);
            break;
        }
    }

    public interface TransferLocation_Interface{
        void onTransferLocation(String location);
    }
}
