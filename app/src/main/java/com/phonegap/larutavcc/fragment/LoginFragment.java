package com.phonegap.larutavcc.fragment;


import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.activity.PerfilActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ImageButton btnRegistro = (ImageButton) view.findViewById(R.id.login_btn_registro);
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistroFragment registroFragment = new RegistroFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragmentContainer, registroFragment)
                        .addToBackStack(null)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).commit();
            }
        });

        ImageButton btnLogin = (ImageButton) view.findViewById(R.id.login_btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PerfilActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        return view;
    }

}
