package com.phonegap.larutavcc.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.phonegap.larutavcc.R;
import com.phonegap.larutavcc.adapter.EventosPasadosAdapter;
import com.phonegap.larutavcc.adapter.EventosProximosAdapter;
import com.phonegap.larutavcc.adapter.PreCachingLayoutManagerForRecyclerView;
import com.phonegap.larutavcc.model.EventosPasadosResponse;
import com.phonegap.larutavcc.model.EventosProximosResponse;
import com.phonegap.larutavcc.util.Constants;
import com.phonegap.larutavcc.util.DeviceUtils;
import com.phonegap.larutavcc.util.TypefaceUtils;

import java.util.LinkedHashSet;
import java.util.List;

/**
 * Contiene la logica de las tabs para eventos futuros y pasados
 */
public class EventosFragment extends Fragment {

    private int mIdTab;
    private TextView mEmptyView;
    private RecyclerView mRecyclerView;
    private PreCachingLayoutManagerForRecyclerView mLayoutManager;
    private List<EventosPasadosResponse> mPasadosResponseList;
    private List<EventosProximosResponse> mProximosResponseList;

    public static EventosFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(Constants.CURRENT_TAB, page);
        EventosFragment fragment = new EventosFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIdTab = getArguments().getInt(Constants.CURRENT_TAB);
//        mPasadosResponseList = Singleton.getInstance().getEventosPasadosResponseList();
//        mProximosResponseList = Singleton.getInstance().getEventosProximosResponseList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_eventos, container, false);
        TypefaceUtils.applyFont(getActivity(), view);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.eventos_listview);
        mEmptyView = (TextView) view.findViewById(R.id.empty_view);

        switch (mIdTab) {
            case 0:
                if (mProximosResponseList != null && !mProximosResponseList.isEmpty()) {
                    mEmptyView.setVisibility(View.GONE);
                    LinkedHashSet <Object> eventosListaMeses = setMesesEventosProximos();
                    EventosProximosAdapter eventosProximosAdapter = new EventosProximosAdapter(getActivity(), eventosListaMeses);
                    setListaEventos();
                    mRecyclerView.setAdapter(eventosProximosAdapter);
                } else {
                    mEmptyView.setVisibility(View.VISIBLE);
                }
                break;
            case 1:
                if (mPasadosResponseList != null && !mPasadosResponseList.isEmpty()) {
                    mEmptyView.setVisibility(View.GONE);
                    LinkedHashSet <Object> eventosListaMeses = setMesesEventosPasados();
                    EventosPasadosAdapter eventosPasadosAdapter = new EventosPasadosAdapter(getActivity(), eventosListaMeses);
                    setListaEventos();
                    mRecyclerView.setAdapter(eventosPasadosAdapter);
                } else {
                    mEmptyView.setVisibility(View.VISIBLE);
                }
                break;
        }
        return view;
    }

    /**
     * Inyectamos la lista dependiendo la categoria elegida
     */
    private void setListaEventos() {
        //Setup layout manager
        mLayoutManager = new PreCachingLayoutManagerForRecyclerView(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.setExtraLayoutSpace(DeviceUtils.getScreenHeight(getActivity()));

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private LinkedHashSet<Object> setMesesEventosProximos(){
        LinkedHashSet<Object> list = new LinkedHashSet<>();
        for(EventosProximosResponse eventoProximo: mProximosResponseList){
            String tituloEventoMes = DeviceUtils.getMonthAndYearFromDateString(getActivity(), eventoProximo.getDate());
            if(DeviceUtils.checkStringNullOrEmpty(tituloEventoMes)){
                list.add(tituloEventoMes);
                list.add(eventoProximo);
            }
        }
        return list;
    }

    private LinkedHashSet<Object> setMesesEventosPasados(){
        LinkedHashSet<Object> list = new LinkedHashSet<>();
        for(EventosPasadosResponse eventoPasado: mPasadosResponseList){
            String tituloEventoMes = DeviceUtils.getMonthAndYearFromDateString(getActivity(), eventoPasado.getDate());
            if(DeviceUtils.checkStringNullOrEmpty(tituloEventoMes)){
                list.add(tituloEventoMes);
                list.add(eventoPasado);
            }
        }
        return list;
    }
}
