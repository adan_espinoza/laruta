package com.phonegap.larutavcc.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.phonegap.larutavcc.model.DirectorioResponse;
import com.phonegap.larutavcc.model.UbicacionesResponse;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by snakelogan on 5/25/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String LOG_TAG = DatabaseHelper.class.getSimpleName();
    //DATABASE
    private static final String DB_NAME = "RutaVinoDB";
    private static final int DB_VERSION = 1;

    //Tables
    private static final String TABLE_LOCATIONS = "TABLE_LOCATIONS";
    private static final String TABLE_LOCATIONS_EN = "TABLE_LOCATIONS_EN";
    private static final String TABLE_DIRECTORIO = "TABLE_DIRECTORIO";
    private static final String TABLE_VINOS = "TABLE_VINOS";
    //Common column
    private static final String KEY_ID = "id";
    private static final String KEY_ID_SERVICE = "id_service";
    private static final String KEY_NAME = "name";

    //Columns Table Directorio
    private static final String KEY_DESCRIPTION_LONG = "description_long";
    private static final String KEY_DESCRIPTION_LONG_EN = "description_long_en";
    private static final String KEY_DESCRIPTION_SHORT = "description_short";
    private static final String KEY_DESCRIPTION_SHORT_EN = "description_short_en";
    private static final String KEY_WEBSITE = "website";
    private static final String KEY_FACEBOOK = "fbk_name";
    private static final String KEY_TELEPHONE = "telephone";
    private static final String KEY_MAP_LAT = "map_lat";
    private static final String KEY_MAP_LON = "map_lon";
    private static final String KEY_REFERENCE = "reference";
    private static final String KEY_RECOMMENDATION = "recommendation";
    private static final String KEY_SHOW_GALERY = "show_galery";
    private static final String KEY_SHOW_WINE = "show_wine";
    private static final String KEY_LOCATIONS = "locations";
    private static final String KEY_PATH = "path";
    private static final String KEY_FILE = "file";
    private static final String KEY_COST = "cost";
    private static final String KEY_COST_EN = "cost_en";
    private static final String KEY_CATEGORY_ID = "category_id";
    private static final String KEY_CATEGORY_NAME = "category_name";
    private static final String KEY_SHOWING_DIRECTIONS_BUTTON = "showing_directions_button";

    //TABLA VINOS
    private static final String KEY_POI_ID = "poi_id";
    private static final String KEY_VINO_ID = "id_vino";
    private static final String KEY_DESCRIPTION = "description";

    //TABLE CREATE STATEMENTS
    private static final String CREATE_TABLE_LOCATIONS =
            "CREATE TABLE " + TABLE_LOCATIONS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    KEY_ID_SERVICE + " TEXT," +
                    KEY_NAME + " TEXT)";

    private static final String CREATE_TABLE_LOCATIONS_EN =
            "CREATE TABLE " + TABLE_LOCATIONS_EN + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    KEY_ID_SERVICE + " TEXT," +
                    KEY_NAME + " TEXT)";

    private static final String CREATE_TABLE_VINOS =
            "CREATE TABLE " + TABLE_VINOS + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    KEY_ID_SERVICE + " TEXT," +
                    KEY_POI_ID + " TEXT," +
                    KEY_VINO_ID + " TEXT," +
                    KEY_DESCRIPTION + " TEXT)";

    private static final String CREATE_TABLE_DIRECTORIO =
            "CREATE TABLE " + TABLE_DIRECTORIO + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    KEY_ID_SERVICE + " TEXT," +
                    KEY_NAME + " TEXT," +
                    KEY_DESCRIPTION_LONG + " TEXT," +
                    KEY_DESCRIPTION_LONG_EN + " TEXT," +
                    KEY_DESCRIPTION_SHORT + " TEXT," +
                    KEY_DESCRIPTION_SHORT_EN + " TEXT," +
                    KEY_WEBSITE + " TEXT," +
                    KEY_FACEBOOK + " TEXT," +
                    KEY_TELEPHONE + " TEXT," +
                    KEY_MAP_LAT + " TEXT," +
                    KEY_MAP_LON + " TEXT," +
                    KEY_REFERENCE + " TEXT," +
                    KEY_RECOMMENDATION + " TEXT," +
                    KEY_SHOW_GALERY + " TEXT," +
                    KEY_SHOW_WINE + " TEXT," +
                    KEY_LOCATIONS + " TEXT," +
                    KEY_PATH + " TEXT," +
                    KEY_FILE + " TEXT," +
                    KEY_COST + " TEXT," +
                    KEY_COST_EN + " TEXT," +
                    KEY_CATEGORY_ID + " TEXT"+
                    KEY_CATEGORY_NAME + " TEXT"+
                    KEY_SHOWING_DIRECTIONS_BUTTON + " TEXT)";


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_LOCATIONS);
        db.execSQL(CREATE_TABLE_LOCATIONS_EN);
        db.execSQL(CREATE_TABLE_DIRECTORIO);
        db.execSQL(CREATE_TABLE_VINOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIRECTORIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATIONS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VINOS);
        onCreate(db);
    }

    public boolean validateIfTableDirectorioExists() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + TABLE_DIRECTORIO + "'", null);
        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public void insertDirectorioRow(DirectorioResponse directorioResponses){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_ID_SERVICE, directorioResponses.getId());
        values.put(KEY_NAME, directorioResponses.getName());
        values.put(KEY_DESCRIPTION_LONG, directorioResponses.getDescriptionLong());
        values.put(KEY_DESCRIPTION_LONG_EN, directorioResponses.getDescriptionLongEn());
        values.put(KEY_DESCRIPTION_SHORT, directorioResponses.getShortDescription());
        values.put(KEY_DESCRIPTION_SHORT_EN, directorioResponses.getShortDescriptionEn());
        values.put(KEY_WEBSITE, directorioResponses.getWebsite());
        values.put(KEY_FACEBOOK, directorioResponses.getFbkName());
        values.put(KEY_TELEPHONE, directorioResponses.getTelephone());
        values.put(KEY_MAP_LAT, directorioResponses.getMapLat());
        values.put(KEY_MAP_LON, directorioResponses.getMapLon());
        values.put(KEY_REFERENCE, directorioResponses.getReference());
        values.put(KEY_RECOMMENDATION, directorioResponses.getRecommendation());
        values.put(KEY_SHOW_GALERY, directorioResponses.getShowGallery());
        values.put(KEY_SHOW_WINE, directorioResponses.getShowWine());
        values.put(KEY_LOCATIONS, directorioResponses.getLocations());
        values.put(KEY_PATH, directorioResponses.getPath());
        values.put(KEY_FILE, directorioResponses.getFile());
        values.put(KEY_COST, directorioResponses.getCost());
        values.put(KEY_COST_EN, directorioResponses.getCostEn());
        values.put(KEY_CATEGORY_ID, directorioResponses.getCategoryId());
        values.put(KEY_CATEGORY_NAME, directorioResponses.getCategoryName());
        values.put(KEY_SHOWING_DIRECTIONS_BUTTON, directorioResponses.getIsShowingDirectionsButton());

        db.insert(TABLE_DIRECTORIO, null, values);
    }

    public void insertLocationRow(UbicacionesResponse ubicacionesResponse){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_SERVICE, ubicacionesResponse.getId());
        values.put(KEY_NAME, ubicacionesResponse.getName());
        db.insert(TABLE_LOCATIONS, null, values);
    }

    public void insertLocationEnRow(UbicacionesResponse ubicacionesResponse){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID_SERVICE, ubicacionesResponse.getId());
        values.put(KEY_NAME, ubicacionesResponse.getName());
        db.insert(TABLE_LOCATIONS_EN, null, values);
    }

    public List<UbicacionesResponse> getLocationsFromDB(){
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM ");
        if(Locale.getDefault().getLanguage().equals("sp")){
            query.append(TABLE_LOCATIONS);
        }else {
            query.append(TABLE_LOCATIONS_EN);
        }
        Log.d(LOG_TAG, query.toString());

        List<UbicacionesResponse> ubicacionesResponseList = new ArrayList<UbicacionesResponse>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor result =  db.rawQuery(query.toString(), null );
        result.moveToFirst();

        while(result.isAfterLast() == false){
            UbicacionesResponse ubiRes = new UbicacionesResponse();
            ubiRes.setId(result.getString(result.getColumnIndex(KEY_ID_SERVICE)));
            ubiRes.setName(result.getString(result.getColumnIndex(KEY_NAME)));
            ubicacionesResponseList.add(ubiRes);
            result.moveToNext();
        }
        return ubicacionesResponseList;
    }

    public List<DirectorioResponse> getDirectorioFromBD(String locationParameter){
        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM ");
        query.append(TABLE_DIRECTORIO);
        query.append("WHERE ");
        query.append(KEY_LOCATIONS);
        query.append(" = ");
        query.append(locationParameter);

        Log.d(LOG_TAG, query.toString());

        List<DirectorioResponse> directorioResponseList = new ArrayList<DirectorioResponse>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor result =  db.rawQuery(query.toString(), null);
        result.moveToFirst();

        while(result.isAfterLast() == false){
            DirectorioResponse dirResp = new DirectorioResponse();
            dirResp.setId(result.getString(result.getColumnIndex(KEY_ID_SERVICE)));
            dirResp.setName(result.getString(result.getColumnIndex(KEY_NAME)));
            directorioResponseList.add(dirResp);
            result.moveToNext();
        }
        return directorioResponseList;
    }
}
